import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { StatisticRoutesModule } from "./statistic.routes";
import { PageAdminStatistic } from "./ui/pages/index/component";
import { UDCardModule } from "app/modules/ud-ui/components/card/card.module";
import { UDNumberSpacingModule } from "app/modules/ud-ui/pipes/number-spacing/module";
import { UDSpinnerModule } from "app/modules/ud-ui/components/spinner/module";
import { ChartModule } from "primeng/chart";
import { UDFormsModule } from "app/modules/ud-forms/ud-forms.module";
import { Ng2FlatpickrModule } from "ng2-flatpickr";
import { StatisticRepository } from "app/modules/statistic/domain/repositories/StatisticRepository";
import { StatisticResource } from "app/modules/statistic/domain/resources/StatisticResource";

@NgModule({
    providers: [
        StatisticRepository,
        StatisticResource
    ],
    imports: [
        CommonModule,
        StatisticRoutesModule,
        UDCardModule,
        UDNumberSpacingModule,
        UDFormsModule,
        UDSpinnerModule,
        ChartModule,
        Ng2FlatpickrModule
    ],
    declarations: [
        PageAdminStatistic
    ],
   
})
export class StatisticModule {
}
