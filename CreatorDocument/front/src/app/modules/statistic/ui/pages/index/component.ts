import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Russian } from "app/libs/flatpickr/FlatpickrRussian";
import { StatisticRepository } from "app/modules/statistic/domain/repositories/StatisticRepository";
import { Statistic } from "app/modules/statistic/domain/interfaces/Statistic";
const moment = require('moment');

const validationMessages = require('./validation-messages.json');

@Component({
    selector: 'statistic',
    templateUrl: './template.html',
    styleUrls: ['./styles.scss']
})
export class PageAdminStatistic implements OnInit {
  public loading: boolean = false;
  public chartData: any;
  public guestsData: any;
  public filterForm: FormGroup;
  public showFormErrors: boolean = false;
  public data;
  public calendarOptionsFrom: any;
  public calendarOptionsUntil: any;

  public validationMessages = validationMessages;

  private fieldsRequirement = {
    from: true,
    until: true
  };

  constructor(private statisticRepository: StatisticRepository,
              private fb: FormBuilder) {}

  ngOnInit() {
    this.filterForm = this.buildForm();
    this.setupDatePickersConfig();
    this.loadStatistic();
    this.loadGuestStatistics();
  }

  public onSubmit($event) {
    $event.preventDefault();
    if (this.filterForm.valid) {
      this.loadStatistic();
      this.loadGuestStatistics();
    } else {
      this.showFormErrors = true;
    }
  }

  public isRequired(fieldName): boolean {
    return this.fieldsRequirement[fieldName];
  }

  private setupDatePickersConfig() {
    this.calendarOptionsFrom = {
      locale: Russian,
      dateFormat: 'd.m.Y',
      enableTime: false,
      defaultDate: moment.utc().subtract(7, 'days').local().format('DD.MM.YYYY'),
      multiple: false
    };

    this.calendarOptionsUntil = {
      locale: Russian,
      dateFormat: 'd.m.Y',
      enableTime: false,
      defaultDate: moment.utc().local().format('DD.MM.YYYY'),
      multiple: false
    };
  }

  private loadStatistic(): void { 

    const fValue = this.filterForm.value;
    const filters = {
      from: this.decodeDate(fValue.from),
      until: this.decodeDate(fValue.until)
    };

    this.loading = true;
    this.statisticRepository.search(filters)
      .finally(() => this.loading = false)
      .subscribe((statistic: Statistic) => {
        
        const labels = statistic.payments.map((si) => {
          return moment(si.date, 'YYYY-MM-DDTHH:mm:ss').format('DD.MM.YYYY');
        });
        this.chartData = {
          labels,
          datasets: [
            {
              label: 'Доход',
              data: statistic.payments.map((si) => {
                return si.sum;
              }),
              fill: false,
              borderColor: '#4ECDC4',
              lineTension: 0,
              pointRadius: 8,
              pointHoverRadius: 10
            }
          ]
        };
      });
  }

  
  private loadGuestStatistics(): void { 

    const fValue = this.filterForm.value;
    const filters = {
      from: this.decodeDate(fValue.from),
      until: this.decodeDate(fValue.until)
    };

    this.loading = true;
    this.statisticRepository.search(filters)
      .finally(() => this.loading = false)
      .subscribe((statistic: Statistic) => {
        
        const labels = statistic.uniqGuests.map((si) => {
          return moment(si.date, 'YYYY-MM-DDTHH:mm:ss').format('DD.MM.YYYY');
        });
        this.guestsData = {
          labels,
          datasets: [
            {
              label: 'Колличество посещений',
              data: statistic.uniqGuests.map((si) => {
                return si.count;
              }),
              fill: false,
              borderColor: '#4ECDC4',
              lineTension: 0,
              pointRadius: 8,
              pointHoverRadius: 10
            }
          ]
        };
      });
  }



  
  private decodeDate(date: Date[] | string | null): any {
    if (date != null) {
      if (typeof date === 'string') {
        return moment(date, 'DD.MM.YYYY').format('YYYY-MM-DDTHH:mm:ss');
      }
      return moment(date[0]).format('YYYY-MM-DDTHH:mm:ss');
    }
    return date;
  }

  private buildForm() {
    return this.fb.group({
      from: [moment.utc().subtract(7, 'days').local().format('DD.MM.YYYY'), Validators.required],
      until: [moment.utc().local().format('DD.MM.YYYY'), Validators.required],
    });
  }
}
