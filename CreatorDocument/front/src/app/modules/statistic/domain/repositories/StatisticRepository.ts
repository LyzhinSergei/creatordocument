import { Injectable } from "@angular/core";
import { BaseRepository } from "app/infrastructure/BaseRepository";
import { StatisticResource } from "app/modules/statistic/domain/resources/StatisticResource";

@Injectable()
export class StatisticRepository extends BaseRepository {
  constructor(statisticResource: StatisticResource) {
    super(statisticResource);
  }
}
