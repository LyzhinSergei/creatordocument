export interface StatisticItem {
    date: string
    sum: number,
    count: number
  }
  
  export interface Statistic {
    payments: StatisticItem[];
    uniqGuests: StatisticItem[] ;
  }
  
