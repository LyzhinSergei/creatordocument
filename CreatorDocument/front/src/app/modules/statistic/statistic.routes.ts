import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PageAdminStatistic } from "app/modules/statistic/ui/pages/index/component";


const routes: Routes = [
  {
    path: '',
    component: PageAdminStatistic
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StatisticRoutesModule {
}