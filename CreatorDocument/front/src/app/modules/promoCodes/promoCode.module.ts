import { ModuleWithProviders, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PromoCodeRouteModule } from "./promoCode.routes";
import { PromoCodeRepository } from './domain/repositories/PromoCodeRepository';
import { PromoCodeResource } from './domain/resources/PromoCodeResource';
import { UDPerPagePaginationModule } from "app/modules/ud-ui/components/per-page-pagination/module";
import { UDDateTimeModule } from "app/modules/ud-ui/pipes/date-time/module";
import { UDKeyPressModule } from "app/modules/ud-ui/directives/key-press/module";
import { UDEmptyTableModule } from "app/modules/ud-ui/components/empty-table/module";
import { UDSpinnerModule } from "app/modules/ud-ui/components/spinner/module";
import { PromoCodeComponentsPromoCodeForm } from './ui/components/PromoCode-form/component';
import { UDTruncateModule } from '../ud-ui/pipes/truncate/module';
import { UDBreadcrumbsModule } from "app/modules/ud-ui/components/breadcrumbs/module";
import { UDCardModule } from "app/modules/ud-ui/components/card/card.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { UDFormsModule } from "app/modules/ud-forms/ud-forms.module";
import { SharedModule } from "app/modules/shared/shared.module";
import { InputMaskModule } from "primeng/inputmask";
import { UDMapModule } from "app/modules/ud-map/module";
import { TabViewModule } from "primeng/tabview";
import { UDFocusModule } from "app/modules/ud-ui/directives/focus/module";
import { DropdownModule } from "primeng/dropdown";
import { PromoCodeFactory } from "app/modules/promoCodes/domain/factories/PromoCodeFactory";
import { PromoCodePagesIndex } from "app/modules/promoCodes/ui/pages/index/component";
import { PromoCodePagesNew } from "app/modules/promoCodes/ui/pages/new/component";
import { PromoCodePagesEdit } from './ui/pages/edit/component';
import { UDActionsDDModule } from "app/modules/ud-ui/components/actions-dd/module";
import { PromoCodeComponentsPromoCodeActions } from './ui/components/PromoCode-actions/component';
import { UDUploadModule } from "app/modules/ud-upload/upload.module";
import { UploadFactory } from '../ud-upload/domain/factories/UploadFactory';
import { Ng2FlatpickrModule } from 'ng2-flatpickr';
import { UDDateTimeInputModule } from '../ud-ui/components/datetime/module';



@NgModule({
  imports: [
    CommonModule,
    PromoCodeRouteModule,
    UDPerPagePaginationModule,
    UDDateTimeModule,
    UDKeyPressModule,
    UDEmptyTableModule,
    UDSpinnerModule,
    UDTruncateModule,
    UDBreadcrumbsModule,
    UDCardModule,
    UDFormsModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    InputMaskModule,
    UDMapModule,
    TabViewModule,
    UDFocusModule,
    DropdownModule,
    UDActionsDDModule,
    UDUploadModule,
    Ng2FlatpickrModule,
    UDDateTimeInputModule
    
  ],
  declarations: [
    PromoCodeComponentsPromoCodeForm,
    PromoCodePagesIndex,
    PromoCodePagesNew,
    PromoCodePagesEdit,
    PromoCodeComponentsPromoCodeActions
  ],
  providers: [
    PromoCodeRepository,
    PromoCodeResource,
    PromoCodeFactory,
    UploadFactory
  ]
})
export class PromoCodeModule {
}