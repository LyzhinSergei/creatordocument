import { Injectable } from "@angular/core";
import { PromoCode } from "../interfaces/PromoCode";

@Injectable()
export class PromoCodeFactory {
  public createEmpty(): PromoCode {
    return {};
  }
}
