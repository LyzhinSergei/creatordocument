import { Injectable } from "@angular/core";
import { BaseRepository } from "app/infrastructure/BaseRepository";
import { PromoCodeResource } from "../resources/PromoCodeResource";

@Injectable()
export class PromoCodeRepository extends BaseRepository {
  constructor(resource: PromoCodeResource) {
    super(resource);
  }
}