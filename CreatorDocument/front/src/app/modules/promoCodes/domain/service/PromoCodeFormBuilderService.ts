import { Injectable, Input } from "@angular/core";
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { PromoCodeRepository } from '../repositories/PromoCodeRepository';
import { PromoCode } from '../interfaces/PromoCode';


@Injectable()
export class PromoCodeFormBuilderService {

  newsForm: FormGroup;
  constructor(private fb: FormBuilder,
    private promoCodeRepository: PromoCodeRepository) {

  }

  public buildForm(promoCode: PromoCode): FormGroup {
    return this.fb.group({
      name: [promoCode.name, [Validators.required, Validators.maxLength(20)]],
      comment: [promoCode.comment, [Validators.required, Validators.maxLength(200)]],
      percent: [promoCode.percent,[Validators.required]],
      end: [promoCode.end,[Validators.required]] 
    })
  }
  
}
