import {PromoCodeStatusEnum} from "../enums/PromoCodeStatusEnum";
export interface PromoCode{
   id?:number
   status?:PromoCodeStatusEnum
   comment?:string
   end?:Date,
   createDate?:Date,
   percent?:number,
   name?:string
}