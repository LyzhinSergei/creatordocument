import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PromoCodePagesIndex } from "app/modules/promoCodes/ui/pages/index/component";
import { PromoCodePagesNew } from "app/modules/promoCodes/ui/pages/new/component";
import { PromoCodePagesEdit } from './ui/pages/edit/component';

const routes: Routes = [
  {
    path: '',
    component: PromoCodePagesIndex
  },
  {
    path: 'new',
    component: PromoCodePagesNew
  },
  {
    path:':id',
    component:PromoCodePagesEdit
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PromoCodeRouteModule {
}