import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { PromoCode } from "../../../domain/interfaces/PromoCode";
import { ActivatedRoute, Router } from "@angular/router";
import { PromoCodeRepository } from "../../../domain/repositories/PromoCodeRepository";
import { ConfirmationService } from "primeng/api";
import { MessageService } from "primeng/components/common/messageservice";
import { removeFromArray } from "app/helpers/immutable";




@Component({
  selector: 'promoCode-pages-index',
  styleUrls: ['./styles.scss'],
  templateUrl: './template.html'
})

export class PromoCodePagesIndex {
  private defaultPaginationParams = { per_page: 10, page: 1 };
  public loading: boolean = false;
  public promoCode: ArrayMeta<PromoCode>;
  public promoCodeItemTotal: number;
  public filters = Object.assign({}, this.defaultPaginationParams, {
    status: 'ACTIVE'
  });

  constructor(private route: ActivatedRoute,
              private router: Router,
              private promoCodeRepository: PromoCodeRepository,
              private confirmationService: ConfirmationService,
              private messageService: MessageService) {

  }

  ngOnInit() {
    const queryParams = this.route.snapshot.queryParams;
    this.changeFiltersAndLoad(queryParams);
  }

  public onPaginationChange(changes): void {
    this.filters = Object.assign({}, this.filters, changes);
    this.changeUrlQueryParams(this.filters);
    this.loadPromoCode();
  }

  public faqTrackByFunc(index, promoCode: PromoCode): number {
    return promoCode.id;
  }

  public nextPage(can: boolean) {
    if (can) {
      this.onPaginationChange({ page: this.filters.page + 1 });
    }
  }

  public prevPage(can: boolean) {
    if (can) {
      this.onPaginationChange({ page: this.filters.page - 1 });
    }
  }

  private changeUrlQueryParams(queryParams): void {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams
    });
  }

  private changeFiltersAndLoad(changes) {
    this.filters = Object.assign({}, this.filters, changes);
    this.loadPromoCode();
  }

  private loadPromoCode(): void {
    this.loading = true;
    this.promoCodeRepository.search(this.filters)
      .finally(() => this.loading = false)
      .subscribe((promoCode: ArrayMeta<PromoCode>) => {
        this.promoCode = promoCode;
        this.promoCodeItemTotal = promoCode.meta.total;
      });
  }

  public onPromoCodeAction(actionType, promoCode, index: number): void {
    switch (actionType) {
      case 'edit':
        this.router.navigate(['./', promoCode.id], { relativeTo: this.route });
        break;
      case 'delete':
        this.showDeleteConfirmationDialog(promoCode, index);
        break;
    }
  }

  private showDeleteConfirmationDialog(promoCode: PromoCode, index: number): void {
    this.confirmationService.confirm({
      header: 'Подтверждение удаления',
      message: `Вы уверены что хотите удалить Промокод "${promoCode.name}"?`,
      acceptLabel: 'Удалить',
      rejectLabel: 'Отмена',
      icon: 'fa fa-trash',
      accept: () => {
        this.promoCodeRepository.delete(promoCode)
          .subscribe(() => {
            this.messageService.add({
              severity: 'success',
              summary: 'Успешно',
              detail: `Промокод ${promoCode.name} удален`
            });
            this.promoCode = removeFromArray(this.promoCode, index);
          }, () => {
            this.messageService.add({
              severity: 'error',
              summary: 'Ошибка',
              detail: `Промокод ${promoCode.name} не был удален`
            });
          });
      }
    });
  }

}