import {
  Component
} from '@angular/core';
import { PromoCode } from "../../../domain/interfaces/PromoCode";
import { ActivatedRoute, Router } from "@angular/router";
import { PromoCodeRepository } from "../../../domain/repositories/PromoCodeRepository";
import { MessageService } from "primeng/components/common/messageservice";
import { PromoCodeFactory } from "app/modules/promoCodes/domain/factories/PromoCodeFactory";


@Component({
  selector: 'promoCode-pages-new',
  styleUrls: ['./styles.scss'],
  templateUrl: './template.html'
})

export class PromoCodePagesNew {
  public promoCode: PromoCode;
  public saving: boolean = false;
  public breadcrumbs: { label: string, link?: any }[] = [
    { label: 'PromoCode', link: '../' },
    { label: 'Новый' }
  ];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private promoCodeRepository: PromoCodeRepository,
              private messageService: MessageService,
              private promoCodeFactory: PromoCodeFactory) {
  }

  ngOnInit() {
    this.promoCode = this.promoCodeFactory.createEmpty();
  }

  public onFormSuccessSubmit($event: PromoCode): void {
    this.saving = true;
    this.promoCodeRepository.save($event)
      .finally(() => this.saving = false)
      .subscribe((promoCode: PromoCode) => {
        this.messageService.add({
          severity: 'success',
          summary: 'Сохранено'
        });
        this.navigateToList();
      });
  }

  public onCancel() {
    this.navigateToList();
  }

  private navigateToList() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}