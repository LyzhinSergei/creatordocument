import {
    Component
  } from '@angular/core';
  import { PromoCode } from "../../../domain/interfaces/PromoCode";
  import { ActivatedRoute, Router } from "@angular/router";
  import { PromoCodeRepository } from "../../../domain/repositories/PromoCodeRepository";
  import { MessageService } from "primeng/components/common/messageservice";
  import { LoginService } from "app/modules/auth/domain/services/LoginService";

  
  
  @Component({
    selector: 'promoCode-pages-edit',
    styleUrls: ['./styles.scss'],
    templateUrl: './template.html'
  })
  
  export class PromoCodePagesEdit {
    public breadcrumbs = [
        { label: 'PromoCode', link: '../' }
      ];
    
      public promoCode: PromoCode;
      public loading: boolean = false;
      public saving: boolean = false;
    
      constructor(private promoCodeRepository: PromoCodeRepository,
                  private messageService: MessageService,
                  private router: Router,
                  private route: ActivatedRoute,
                  private loginService: LoginService) {
      }
    
      ngOnInit() {
        this.loadAndSetupPromoCode();
      }
    
      public onFormSuccessSubmit($event: PromoCode): void {
        this.saving = true;
        this.promoCodeRepository.save($event)
          .finally(() => this.saving = false)
          .subscribe((faq: PromoCode) => {
              this.messageService.add({
                severity: 'success',
                summary: 'Сохранено',
                detail: `Промокод ${this.promoCode.name} сохранен`
              });
              this.navigateToList();
            });
      }
    
      public onCancel() {
        this.navigateToList();
      }
    
      private navigateToList() {
        this.router.navigate(['../'], { relativeTo: this.route });
      }
    
      private loadAndSetupPromoCode(): void {
        const promoCodeId = this.route.snapshot.params['id'];
        this.loading = true;
    
        this.promoCodeRepository.loadById(promoCodeId)
          .finally(() => this.loading = false)
          .subscribe((promoCode: PromoCode) => {
            this.promoCode = promoCode;
            this.breadcrumbs.push({ label: promoCode.name, link: null });
          });
      }
  }