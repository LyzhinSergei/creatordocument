import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PromoCode } from "../../../domain/interfaces/PromoCode";
import { ActivatedRoute, Router } from "@angular/router";
import { PromoCodeRepository } from "../../../domain/repositories/PromoCodeRepository";
import { FormArray,FormBuilder, FormGroup } from "@angular/forms";
const validationMessages = require('./validation-messages.json');
import { UploadFactory } from 'app/modules/ud-upload/domain/factories/UploadFactory';
import { PromoCodeFormBuilderService } from 'app/modules/promoCodes/domain/service/PromoCodeFormBuilderService';

@Component({
  selector: 'promoCode-components-promoCode-form',
  styleUrls: ['./styles.scss'],
  templateUrl: './template.html',
  providers: [PromoCodeFormBuilderService]
})

export class PromoCodeComponentsPromoCodeForm implements OnInit {
  @Input() promoCode: PromoCode;
  @Input() loading: boolean = false;
  @Input() disabled: boolean = false;
  @Output() cancel = new EventEmitter<{ originalEvent: Event }>();
  @Output() successSubmit = new EventEmitter<PromoCode>();

  public showFormErrors: boolean = false;
  public promoCodeForm: FormGroup;
  public validationMessages = validationMessages;
  private fieldsRequirement = {
    comment: true,
    name: true,
    percent: true,
    end: true
  };
  public uploadTempModel: any;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private promoCodeRepository: PromoCodeRepository,
              private fb: FormBuilder,
              private promoCodeFormBuilderService: PromoCodeFormBuilderService
                                      ) {
  }

  ngOnInit() {
    this.promoCodeForm = this.promoCodeFormBuilderService.buildForm(this.promoCode);
  }


  public onCancelClicked($event): void {
    $event.preventDefault();
    this.cancel.emit({ originalEvent: $event });
  }
  
 
  
  public onSubmit($event): void {
    $event.preventDefault();
    this.sendSubmitEvent();
  }

  private sendSubmitEvent() {
    if (this.promoCodeForm.valid) {
      const promoCode = Object.assign({}, this.promoCode, this.promoCodeForm.value, {});
      this.successSubmit.emit(promoCode);
    } else {
      this.showFormErrors = true;
    }
  }
  public isRequired(fieldName): boolean {
    return this.fieldsRequirement[fieldName];
  }
  

}