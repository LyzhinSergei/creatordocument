import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { PromoCode } from "../../../domain/interfaces/PromoCode";
import { PromoCodeStatusEnum } from "../../../domain/enums/PromoCodeStatusEnum";

@Component({
  selector: 'promoCode-components-promoCode-actions',
  templateUrl: './template.html'
})
export class PromoCodeComponentsPromoCodeActions implements OnChanges {
  @Input() promoCode: PromoCode;
  @Output() action = new EventEmitter<string>();

  public possibleActions = [];

  ngOnChanges(changes: SimpleChanges) {
    if (changes['promoCode']) {
      this.setupPossibleActions();
    }
  }

  public onPromoCodeAction($event): void {
    this.action.emit($event.item.value);
  }

  private setupPossibleActions() {

    this.possibleActions = [
      { label: 'Редактировать', type: 'default', value: 'edit' },
    ];
    this.possibleActions.push({ label: 'Деактивировать', type: 'danger', value: 'delete' });
  }
}
