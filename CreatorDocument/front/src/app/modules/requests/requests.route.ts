import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RequestPagesIndex } from './ui/pages/index/component';
import { RequestPagesView } from './ui/pages/view/component';


const routes: Routes = [
  {
    path: '',
    component: RequestPagesIndex
  },
  {
    path:':id',
    component: RequestPagesView
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RequestsRouteModule {
}
