import { Injectable } from "@angular/core";
import { Request } from "../interfaces/Request";
import { RequestStatusEnum } from '../enums/RequestStatusEnum';
import { ProductHelper } from '../interfaces/ProductHelper';

@Injectable()
export class RequestFactory {
  public createEmpty(): Request {
    return {};
  }
}
