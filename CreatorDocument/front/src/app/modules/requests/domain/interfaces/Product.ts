export interface Product {
    name?: string,
    description?:string,
    purchaseDate?:Date
}