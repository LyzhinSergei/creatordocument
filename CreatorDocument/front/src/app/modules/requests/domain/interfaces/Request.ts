import { RequestStatusEnum } from "../enums/RequestStatusEnum"
import { Product } from './Product';
export interface Request {
   id?: number,
   status?: RequestStatusEnum,
   client?: string,
   type?:string,
   cost?:number,
   productModel?:Product
   isPay?:boolean,
   productId?:number,
   comment?:string,
   delivelerTime?:string,
   state?:string
}
