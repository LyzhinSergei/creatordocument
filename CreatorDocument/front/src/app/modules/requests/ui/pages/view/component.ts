import {
    Component
  } from '@angular/core';
  import { Request } from "../../../domain/interfaces/Request";
  import { ActivatedRoute, Router } from "@angular/router";
  import { RequestRepository } from "../../../domain/repositories/RequestRepository";
  import { MessageService } from "primeng/components/common/messageservice";
  import { LoginService } from "app/modules/auth/domain/services/LoginService";

  
  
  @Component({
    selector: 'request-pages-edit',
    styleUrls: ['./styles.scss'],
    templateUrl: './template.html'
  })
  
  export class RequestPagesView {
    public breadcrumbs = [
        { label: 'Заявки', link: '../' }
      ];
    
      public request: Request;
      public loading: boolean = false;
      public saving: boolean = false;
    
      constructor(private requestRepository: RequestRepository,
                  private messageService: MessageService,
                  private router: Router,
                  private route: ActivatedRoute,
                  private loginService: LoginService) {
      }
    
      ngOnInit() {
        this.loadAndSetupStocks();
      }
    
      
      public onCancel() {
        this.navigateToList();
      }
    
      private navigateToList() {
        this.router.navigate(['../'], { relativeTo: this.route });
      }
    
      private loadAndSetupStocks(): void {
        const requestId = this.route.snapshot.params['id'];
        this.loading = true;
    
        this.requestRepository.loadById(requestId)
          .finally(() => this.loading = false)
          .subscribe((request: Request) => {
            this.request = request;
            this.breadcrumbs.push({ label: request.id.toString(), link: null });
          });
      }
  }