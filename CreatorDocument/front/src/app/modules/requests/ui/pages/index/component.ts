import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Request } from 'app/modules/requests/domain/interfaces/Request';
import { RequestRepository } from 'app/modules/requests/domain/repositories/RequestRepository';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { removeFromArray } from 'app/helpers/immutable';

import * as moment from 'moment';
import { FormBuilder, FormGroup } from '@angular/forms';
import { RequestStatusEnum } from 'app/modules/requests/domain/enums/RequestStatusEnum';

@Component({
  selector: 'request-pages-index',
  templateUrl: './template.html'
})

export class RequestPagesIndex implements OnInit {
  private defaultPaginationParams = { per_page: 10, page: 1 };
  public loading: boolean = false;
  public requests: ArrayMeta<Request>;
  public requestsItemTotal: number;
  public filters: any = Object.assign({}, this.defaultPaginationParams, {
    id: 0,
    type: true,
    state:'',
    status:''
  });
  public filterForm: FormGroup;
  public showFormErrors: boolean = false;
  public RequestStatusEnum = RequestStatusEnum;

  public statusDropBox = [
    { label: 'Неоплаченные', value: "NOT_SUCCESS" },
    { label: 'Оплаченные', value: "SUCCESS" }
  ];

  public typeDropBox = [
    { label: 'Посылка', value: false },
    { label: 'Квест', value: true }
  ];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private requestRepository: RequestRepository,
              private confirmationService: ConfirmationService,
              private messageService: MessageService,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.filterForm = this.buildForm();
    const queryParams = this.route.snapshot.queryParams;
    this.changeFiltersAndLoad(queryParams);
  }

  public onPaginationChange(changes): void {
    this.filters = Object.assign({}, this.filters, changes);
    this.changeUrlQueryParams(this.filters);
    this.loadRequests();
  }

  public requestsTrackByFunc(index, requests: Request): number {
    return requests.id;
  }

  public nextPage(can: boolean) {
    if (can) {
      this.onPaginationChange({ page: this.filters.page + 1 });
    }
  }

  public prevPage(can: boolean) {
    if (can) {
      this.onPaginationChange({ page: this.filters.page - 1 });
    }
  }

  private changeUrlQueryParams(queryParams): void {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams
    });
  }

  private changeFiltersAndLoad(changes) {
    this.filters = Object.assign({}, this.filters, changes);
    this.loadRequests();
  }

  private loadRequests(): void {

    const fValue = this.filterForm.value;
    this.filters.type = fValue.type;
    this.filters.id = fValue.id;
    this.filters.status = fValue.status;

    this.loading = true;
    this.requestRepository.search(this.filters)
      .finally(() => this.loading = false)
      .subscribe((requests: ArrayMeta<Request>) => {
        this.requests = requests;
        this.requestsItemTotal = requests.meta.total;
      });
  }

  private decodeDate(date: Date[] | string | null): any {
    if (date != null) {
      if (typeof date === 'string') {
        return moment(date, 'DD.MM.YYYY').format('YYYY-MM-DDTHH:mm:ss');
      }
      return moment(date[0]).format('YYYY-MM-DDTHH:mm:ss');
    }
    return date;
  }

  public onRequestAction(actionType, request, index: number): void {
    switch (actionType) {
      case 'edit':
        this.router.navigate(['./', request.id], { relativeTo: this.route });
        break;
      case 'done':
        this.showDoneConfirmationDialog(request, index);
        break;
    }
  }

  private showDoneConfirmationDialog(request: Request, index: number) {
    request.state = 'DONE';
    this.confirmationService.confirm({
      header: 'Подтверждение завершения',
      message: `Вы уверены что хотите завершить заявку "${request.id}"?`,
      acceptLabel: 'Завершить',
      rejectLabel: 'Отмена',
      icon: 'fa fa-trash',
      accept: () => {
        this.requestRepository.update(request)
          .subscribe(() => {
            this.messageService.add({
              severity: 'success',
              summary: 'Успешно',
              detail: `Заявка ${request.id} завершена`
            });
            this.requests = removeFromArray(this.requests, index);
          }, () => {
            this.messageService.add({
              severity: 'error',
              summary: 'Ошибка',
              detail: `Заявка ${request.id} не была завершена`
            });
          });
      }
    });
  }

  public onSubmit($event) {
    $event.preventDefault();
    this.loadRequests();

  }

  private buildForm() {
    return this.fb.group({
      id: 0,
      type: true,
      state:'',
      status:''
    });
  }

  public onFiltersChange(changes): void {
    this.filters = Object.assign({}, this.defaultPaginationParams, changes);
    this.changeUrlQueryParams(this.filters);
    this.loadRequests();
  }
}
