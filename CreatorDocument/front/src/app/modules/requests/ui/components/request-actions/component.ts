import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { Request } from "../../../domain/interfaces/Request";

@Component({
  selector: 'request-components-request-actions',
  templateUrl: './template.html'
})
export class RequestComponentsRequestActions implements OnChanges {
  @Input() requests: Request;
  @Input() state: string;
  @Output() action = new EventEmitter<string>();


  public possibleActions = [];

  ngOnChanges(changes: SimpleChanges) {
    if (changes['requests']) {
      this.setupPossibleActions();
    }
  }

  public onRequestAction($event): void {
    this.action.emit($event.item.value);
  }

  private setupPossibleActions() {
    this.possibleActions = [
      { label: 'Просмотреть', type: 'default', value: 'edit' }
    ];
    if (this.state != 'DONE') {
      this.possibleActions.push({ label: 'Завершить', type: 'default', value: 'done' });
    }
  }
}