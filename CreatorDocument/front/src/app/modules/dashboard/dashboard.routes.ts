import { NgModule, Component } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DashboardLayoutsMain } from "./ui/layouts/main/component";


const routes: Routes = [
  {
    path: '',
    component: DashboardLayoutsMain,
    children: [
      { path: "creators", loadChildren: '../creator/creator.module#CreatorModule' },
      { path: 'users', loadChildren: '../users/users.module#UsersModule' }
    ]

  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutesModule {
}
