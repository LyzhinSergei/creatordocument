import { Injectable } from "@angular/core";
import { HttpResource } from 'app/modules/core/infrastructure/HttpResource';
import { BaseRestResource } from 'app/infrastructure/BaseRestResource';

@Injectable()
export class CreatorResource extends BaseRestResource {
  constructor(httpResource: HttpResource) {
    super(httpResource, 'creators');
  }
}