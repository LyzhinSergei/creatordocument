import { Injectable } from "@angular/core";
import { Creator } from "../interfaces/Creator";
import { CreatorStatusEnum } from '../enums/CreatorStatusEnum';

@Injectable()
export class CreatorFactory {
  public createEmpty(): Creator {
    return {
      id: 0,
      status: CreatorStatusEnum.Active,
      name: '',
      link: '',
      bibliography: '',
      conclusion: '',
      main: this.createEmptyMain(),
      introduction: '',
      annotation: '',
      annotationEng: '',
      keysEng: '',
      keys: '',
      autors: this.createEmptyAutors()
    };
  }

  public createEmptyAutors() {
    return [{
      fullname: '',
      email: '',
      work: '',
      photoUpload: '',
      description: '',
      descriptionEng: ''
    }];
  }
  public createEmptyMain() {
    return [{
      id: 0,
      name: '',
      text: ''
    }];
  }
}

