import { Injectable } from "@angular/core";
import { BaseRepository } from 'app/infrastructure/BaseRepository';
import { CreatorResource } from '../resources/CreatorResource';

@Injectable()
export class CreatorRepository extends BaseRepository {
  constructor(resource: CreatorResource) {
    super(resource);
  }
}