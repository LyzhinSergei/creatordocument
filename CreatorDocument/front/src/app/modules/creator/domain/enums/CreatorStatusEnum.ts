export enum CreatorStatusEnum {
  Active = 'ACTIVE',
  Deleted = 'DELETED',
  Blocked = 'BLOCKED'
}