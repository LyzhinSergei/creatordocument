import { CreatorStatusEnum } from '../enums/CreatorStatusEnum';
import { UploadImage } from "app/modules/ud-upload/domain/interfaces/UploadImage";

export interface Creator {
   id?: number;
   status?: CreatorStatusEnum;
   link?: string;
   name?: string;
   bibliography?: string;
   conclusion?: string;
   main?: Main[];
   introduction?: string;
   annotation?: string;
   keys?: string;
   annotationEng?: string;
   keysEng?: string;
   autors?: any | Autors[];
}

export interface Autors{
   id?: number;
   fullname?: string;
   email?: string;
   work?: string;
   photoUpload?: UploadImage;
   description?: string;
   descriptionEng?: string;
} 

export interface Main{
   id?:number;
   name?:string;
   text?:string;
}
