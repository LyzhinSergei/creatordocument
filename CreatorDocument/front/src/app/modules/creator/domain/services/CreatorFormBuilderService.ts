import { Injectable, Input } from "@angular/core";
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { CreatorRepository } from '../repositories/CreatorRepository';
import { Creator } from '../interfaces/Creator';
import { Autors } from '../interfaces/Creator';
import { Main } from '../interfaces/Creator';

@Injectable()
export class CreatorFormBuilderService {

  public creatorForm: FormGroup;
  constructor(private fb: FormBuilder) {

  }

  public buildForm(creator: Creator): FormGroup {
    return this.fb.group({
      id: [creator.id, [Validators.required]],
      status: [creator.status, [Validators.required]],
      link: [creator.link],
      name: [creator.name, [Validators.required]],
      bibliography: [creator.bibliography, [Validators.required]],
      conclusion: [creator.conclusion, [Validators.required]],
      main: this.getArrayOfMain(creator.main),
      introduction: [creator.introduction, [Validators.required]],
      annotation: [creator.annotation, [Validators.required]],
      annotationEng: [creator.annotationEng, [Validators.required]],
      keysEng: [creator.keysEng],
      keys: [creator.keys],
      autors: this.getArrayOfAutors(creator.autors)
    });
  }

  getArrayOfMain(elements: Main[], isRequired: boolean = false) {
    if (!elements) return this.fb.array([
      this.fb.group({
        id: null,
        name: [''],
        text: ['']
      })
    ])
    else {
      const fbArray = this.fb.array([]);
      elements.forEach((element) => {
        fbArray.push(this.fb.group({
          id: [element.id],
          name: [element.name || ''],
          text: [element.text || '']
        }))
      });
      return fbArray;
    }
  }

  // getArrayOfKeys(elements: Keys[], isRequired: boolean = false) {
  //   if (!elements) return this.fb.array([
  //     this.fb.group({
  //       id: null,
  //       key: ['']
  //     })
  //   ])
  //   else {
  //     const fbArray = this.fb.array([]);
  //     elements.forEach((element) => {
  //       fbArray.push(this.fb.group({
  //         id: [element.id],
  //         key: [element.key || '']
  //       }))
  //     });
  //     return fbArray;
  //   }
  // }

  getArrayOfAutors(elements: Autors[], isRequired: boolean = false) {
    if (!elements) return this.fb.array([
      this.fb.group({
        id: null,
        fullname: [''],
        email: [''],
        work: [''],
        description: [''],
        descriptionEng: [''],
        photoUpload: []
      })
    ])
    else {
      const fbArray = this.fb.array([]);
      elements.forEach((element) => {
        fbArray.push(this.fb.group({
          id: [element.id],
          fullname: [element.fullname || ''],
          email: [element.email || ''],
          work: [element.work || ''],
          description: [element.description || ''],
          descriptionEng: [element.descriptionEng || ''],
          photoUpload: [element.photoUpload || []],
        }))
      });
      return fbArray;
    }
  }
}
