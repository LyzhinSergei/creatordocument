import { ModuleWithProviders, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { UDPerPagePaginationModule } from "app/modules/ud-ui/components/per-page-pagination/module";
import { UDDateTimeModule } from "app/modules/ud-ui/pipes/date-time/module";
import { UDKeyPressModule } from "app/modules/ud-ui/directives/key-press/module";
import { UDEmptyTableModule } from "app/modules/ud-ui/components/empty-table/module";
import { UDSpinnerModule } from "app/modules/ud-ui/components/spinner/module";
import { CreatorRouteModule } from './creator.routes';
import { CreatorPagesIndex } from './ui/pages/index/component'
import { CreatorResource } from './domain/resources/CreatorResource';
import { CreatorRepository } from './domain/repositories/CreatorRepository';
import { CreatorFactory } from './domain/factories/CreatorFactory';
import { CreatorComponentsCreatorForm } from './ui/components/creator-form/component';
import { CreatorPagesNew } from './ui/pages/new/component';
import { UDCardModule } from "app/modules/ud-ui/components/card/card.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { UDFormsModule } from "app/modules/ud-forms/ud-forms.module";
import { UDBreadcrumbsModule } from "app/modules/ud-ui/components/breadcrumbs/module";
import { UDDateTimeInputModule } from "app/modules/ud-ui/components/datetime/module";
import { Ng2FlatpickrModule } from "ng2-flatpickr";
import { CreatorPagesEdit } from './ui/pages/edit/component';
import { UDMapModule } from "app/modules/ud-map/module";
import { UDActionsDDModule } from "app/modules/ud-ui/components/actions-dd/module";
import { UDUploadModule } from "app/modules/ud-upload/upload.module";
import { DropdownModule } from "primeng/dropdown";
import { CreatorStatusViewModule } from './ui/components/creator-status-view/module';
import { UDTruncateModule } from "app/modules/ud-ui/pipes/truncate/module";
import { CreatorComponentsCreatorActions } from './ui/components/creator-action/component';
import { CreatorStatusPipe } from './ui/pages/pipes/creator-status';
import { UploadFactory } from '../ud-upload/domain/factories/UploadFactory';
import { SharedModule } from '../shared/shared.module';
import { InputMaskModule } from 'primeng/inputmask';
import { TabViewModule } from 'primeng/tabview';
import { UDFocusModule } from '../ud-ui/directives/focus/module';




@NgModule({
  imports: [
    CommonModule,
    UDPerPagePaginationModule,
    UDDateTimeModule,
    UDKeyPressModule,
    UDEmptyTableModule,
    UDSpinnerModule,
    CreatorRouteModule,
    UDCardModule,
    FormsModule,
    ReactiveFormsModule,
    UDFormsModule,
    UDBreadcrumbsModule,
    UDDateTimeInputModule,
    Ng2FlatpickrModule,
    UDMapModule,
    UDActionsDDModule,
    UDUploadModule,
    DropdownModule,
    CreatorStatusViewModule,
    UDTruncateModule,
    SharedModule,
    InputMaskModule,
    TabViewModule,
    UDFocusModule,
  ],

  declarations: [

    CreatorPagesIndex,
    CreatorPagesNew,
    CreatorPagesEdit,
    CreatorStatusPipe,
    CreatorComponentsCreatorActions,
    CreatorComponentsCreatorForm
  ],
  providers: [
    CreatorResource,
    CreatorRepository,
    CreatorFactory,
    UploadFactory
  ]
})
export class CreatorModule {
}