import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CreatorPagesIndex } from './ui/pages/index/component';
import { CreatorPagesNew } from './ui/pages/new/component';
import { CreatorPagesEdit } from './ui/pages/edit/component';


const routes: Routes = [
  {
    path: '',
    component: CreatorPagesIndex
  },
  {
    path:'new',
    component: CreatorPagesNew
  },
  {
    path:':id',
    component:CreatorPagesEdit
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreatorRouteModule {
}