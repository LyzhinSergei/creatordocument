import {
    Component
  } from '@angular/core';
import { Creator } from "../../../domain/interfaces/Creator";
import { ActivatedRoute, Router } from "@angular/router";
import { CreatorRepository } from "../../../domain/repositories/CreatorRepository";
import { MessageService } from "primeng/components/common/messageservice";
import { CreatorFactory } from '../../../domain/factories/CreatorFactory';


@Component({
    selector: 'creator-pages-new',
    templateUrl: './template.html'
  })

  export class CreatorPagesNew {
    public creator: Creator;
    public saving: boolean = false;
    public breadcrumbs: Array<{ label: string, link?: any }> = [
      { label: 'Документы', link: '../' },
      { label: 'Новый' }
    ];

    constructor(private route: ActivatedRoute,
                private router: Router,
                private creatorRepository: CreatorRepository,
                private messageService: MessageService,
                private creatorFactory: CreatorFactory) {
    }

    ngOnInit() {
      this.creator = this.creatorFactory.createEmpty();
    }

    private navigateToList() {
      this.router.navigate(['../'], { relativeTo: this.route });
    }

    public onFormSuccessSubmit($event: Creator): void {
    this.saving = true;
    this.creatorRepository.create($event)
        .finally(() => this.saving = false)
        .subscribe((creator: Creator) => {
          this.messageService.add({
            severity: 'success',
            summary: 'Сохранено'
          });
          this.navigateToList();
        });
    }
    public onCancelClicked(): void {
      this.navigateToList();
    }

  }
