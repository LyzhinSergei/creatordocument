import { Component } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { removeFromArray } from 'app/helpers/immutable';
import moment from 'moment';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CreatorStatusEnum } from 'app/modules/creator/domain/enums/CreatorStatusEnum';
import { Creator } from 'app/modules/creator/domain/interfaces/Creator';
import { CreatorRepository } from 'app/modules/creator/domain/repositories/CreatorRepository';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'creator-pages-index',
  templateUrl: './template.html'
})

export class CreatorPagesIndex {
  private defaultPaginationParams = { per_page: 10, page: 1 };
  public loading: boolean = false;
  public creator: ArrayMeta<Creator>;
  public creatorItemTotal: number;
  public filters = Object.assign({}, this.defaultPaginationParams, {
    status: CreatorStatusEnum.Active
  });
  public filterForm: FormGroup;
  public showFormErrors: boolean = false;
  public CreatorStatusEnum = CreatorStatusEnum;
  public link: string;
  public fileUrl: any;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private creatorRepository: CreatorRepository,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private fb: FormBuilder,
    private sanitizer: DomSanitizer) {

  }
  public downloadMyFile(path, name) {
    var nameDoc = name;
    if (nameDoc.length >= 50) {
      nameDoc = nameDoc.substring(0, 49);
    }
    this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(path);
    let link = document.createElement("a");
    link.download = name + ".docx";
    link.href = "../assets/doc/" + nameDoc + ".docx";
    link.click();
    link.remove();
  }
  public openMyFile(path, name) {
    var nameDoc = name;
    if (nameDoc.length >= 50) {
      nameDoc = nameDoc.substring(0, 49);
    }
    this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(path);
    let link = document.createElement("a");
    link.href = "../assets/doc/" + nameDoc + ".docx";
    link.click();
    link.remove();
  }

  ngOnInit() {
    this.filterForm = this.buildForm();
    const queryParams = this.route.snapshot.queryParams;
    this.changeFiltersAndLoad(queryParams);
  }

  public onPaginationChange(changes): void {
    this.filters = Object.assign({}, this.filters, changes);
    this.changeUrlQueryParams(this.filters);
    this.loadCreator();
  }

  public questTrackByFunc(index, creator: Creator): number {
    return creator.id;
  }

  public nextPage(can: boolean) {
    if (can) {
      this.onPaginationChange({ page: this.filters.page + 1 });
    }
  }

  public prevPage(can: boolean) {
    if (can) {
      this.onPaginationChange({ page: this.filters.page - 1 });
    }
  }

  private changeUrlQueryParams(queryParams): void {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams
    });
  }

  private changeFiltersAndLoad(changes) {
    this.filters = Object.assign({}, this.filters, changes);
    this.loadCreator();
  }

  private loadCreator(): void {

    const fValue = this.filterForm.value;

    this.loading = true;
    this.creatorRepository.search(this.filters)
      .finally(() => this.loading = false)
      .subscribe((creator: ArrayMeta<Creator>) => {
        this.creator = creator;
        this.creatorItemTotal = creator.meta.total;
      });
  }


  public onCreatorAction(actionType, creator, index: number): void {
    switch (actionType) {
      case 'edit':
        this.router.navigate(['./', creator.id], { relativeTo: this.route });
        break;
      case 'delete':
        this.showDeleteConfirmationDialog(creator, index);
        break;
      case 'blocked':
        this.showBlockedConfirmationDialog(creator, index);
        break;

    }
  }


  private showDeleteConfirmationDialog(creator: Creator, index: number): void {
    creator.status = CreatorStatusEnum.Deleted;
    this.confirmationService.confirm({
      header: 'Подтверждение удаления',
      message: `Вы уверены что хотите удалить документ "${creator.name}"?`,
      acceptLabel: 'Удалить',
      rejectLabel: 'Отмена',
      icon: 'fa fa-trash',
      accept: () => {
        this.creatorRepository.delete(creator)
          .subscribe(() => {
            this.messageService.add({
              severity: 'success',
              summary: 'Успешно',
              detail: `Документ ${creator.name} удален`
            });
            this.creator = removeFromArray(this.creator, index);
          }, () => {
            this.messageService.add({
              severity: 'error',
              summary: 'Ошибка',
              detail: `Докмуент ${creator.name} не был удален`
            });
          });
      }
    });
  }


  private showBlockedConfirmationDialog(creator: Creator, index: number) {
    creator.status = CreatorStatusEnum.Deleted;
    this.confirmationService.confirm({
      header: 'Подтверждение отклонения',
      message: `Вы уверены что хотите заблокировать документ "${creator.name}"?`,
      acceptLabel: 'Заблокировать',
      rejectLabel: 'Отмена',
      icon: 'fa fa-trash',
      accept: () => {
        this.creatorRepository.update(creator)
          .subscribe(() => {
            this.messageService.add({
              severity: 'success',
              summary: 'Успешно',
              detail: `Документ ${creator.name} заблокирован`
            });
            this.creator = removeFromArray(this.creator, index);
          }, () => {
            this.messageService.add({
              severity: 'error',
              summary: 'Ошибка',
              detail: `Документ ${creator.name} не был заблокирован`
            });
          });
      }
    });
  }

  public onSubmit($event) {
    $event.preventDefault();
    this.loadCreator();

  }

  private buildForm() {
    return this.fb.group({
      status: ['']
    });
  }

  public onFiltersChange(changes): void {
    this.filters = Object.assign({}, this.defaultPaginationParams, changes);
    this.changeUrlQueryParams(this.filters);
    this.loadCreator();
  }

}