import { PipeTransform, Pipe } from '@angular/core';
import { CreatorStatusEnum } from "app/modules/creator/domain/enums/CreatorStatusEnum";

@Pipe({ name: 'creatorStatus' })
export class CreatorStatusPipe implements PipeTransform {
  transform(status: CreatorStatusEnum): string {
    switch (status) {
      case CreatorStatusEnum.Deleted:
        return 'Удалено';
      case CreatorStatusEnum.Active:
        return 'Активный';
      case CreatorStatusEnum.Blocked:
        return 'Заблокированный';
      default:
        return 'Неизвестный';
    }
  }
}