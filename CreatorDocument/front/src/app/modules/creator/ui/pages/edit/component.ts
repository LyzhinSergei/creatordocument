import {
  Component
} from '@angular/core';
import { Creator } from "../../../domain/interfaces/Creator";
import { ActivatedRoute, Router } from "@angular/router";
import { CreatorRepository } from "../../../domain/repositories/CreatorRepository";
import { MessageService } from "primeng/components/common/messageservice";
import { CreatorFactory } from 'app/modules/posters/domain/factories/CreatorFactory';
import { LoginService } from "app/modules/auth/domain/services/LoginService";

@Component({
  selector: 'creator-pages-edit',
  templateUrl: './template.html'
})

export class CreatorPagesEdit {
  public breadcrumbs = [
    { label: 'Документы', link: '../' }
  ];

  public creator: Creator;
  public loading: boolean = false;
  public saving: boolean = false;

  constructor(private creatorRepository: CreatorRepository,
              private messageService: MessageService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.loadAndSetupCreator();
  }

  public onFormSuccessSubmit($event: Creator): void {
    this.saving = true;
    this.creatorRepository.save($event)
      .finally(() => this.saving = false)
      .subscribe((creator: Creator) => {
        this.messageService.add({
          severity: 'success',
          summary: 'Сохранено',
          detail: `Документ ${creator.name} сохраненён`
        });
        this.navigateToList();
      });
  }

  public onCancel() {
    this.navigateToList();
  }

  private navigateToList() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  private loadAndSetupCreator(): void {
    const creatorId = this.route.snapshot.params['id'];
    this.loading = true;

    this.creatorRepository.loadById(creatorId)
    
      .finally(() => this.loading = false)
      .subscribe((creator: Creator) => {
        this.creator = creator;
        this.breadcrumbs.push({ label: creator.name, link: null });
      });
  }
}
