import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CreatorComponentsCreatorStatusView } from "./component";
import { UDColoredLabelModule } from "app/modules/ud-ui/components/colored-label/module";

@NgModule({
  imports: [CommonModule, UDColoredLabelModule],
  declarations: [CreatorComponentsCreatorStatusView],
  exports: [CreatorComponentsCreatorStatusView]
})
export class CreatorStatusViewModule {
}