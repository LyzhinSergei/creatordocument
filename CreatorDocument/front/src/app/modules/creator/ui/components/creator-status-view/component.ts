import { Component, Input } from '@angular/core';
import { CreatorStatusEnum } from "app/modules/creator/domain/enums/CreatorStatusEnum";
import { Creator } from 'app/modules/creator/domain/interfaces/Creator';


@Component({
  selector: 'creator-components-creator-status-view',
  templateUrl: './template.html'
})
export class CreatorComponentsCreatorStatusView {
  @Input() creator: Creator;
  @Input() size = 'sm';

  public statusViews = {
    [CreatorStatusEnum.Active]: { label: 'Активный', color: 'ACTIVE' },
    [CreatorStatusEnum.Deleted]: { label: 'Удаленный', color: 'DELETED' },
    [CreatorStatusEnum.Blocked]: { label: 'Заблокированный', color: 'BLOCKED' }
  };

  public get color(): string {
    return this.statusViews[this.creator.status].color;
  }

  public get label(): string {
    return `${this.statusViews[this.creator.status].label}`;
  }
}