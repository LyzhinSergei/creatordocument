import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Creator } from "../../../domain/interfaces/Creator";
import { FormBuilder, FormGroup, FormArray, AbstractControl, ValidatorFn, FormControl } from "@angular/forms";
import { FlatpickrOptions } from 'ng2-flatpickr';
import { BehaviorSubject, Subject } from 'rxjs';
import { UploadImage } from 'app/modules/ud-upload/domain/interfaces/UploadImage';
import { UploadFile } from 'app/modules/ud-upload/domain/interfaces/UploadFile';
import { UploadFileRepository } from 'app/modules/ud-upload/domain/repositories/UploadFileRepository';
import { removeFromArray } from 'app/helpers/immutable';
import { UploadVideo } from 'app/modules/ud-upload/domain/interfaces/UploadVideo';
import { CreatorFormBuilderService } from 'app/modules/creator/domain/services/CreatorFormBuilderService';
const validationMessages = require('./validation-messages.json');

@Component({
  selector: 'creator-components-creator-form',
  styleUrls: ['./styles.scss'],
  templateUrl: './template.html',
  providers: [CreatorFormBuilderService]
})

export class CreatorComponentsCreatorForm implements OnInit {
  @Input() creator: Creator;
  @Input() loading: boolean = false;
  @Input() disabled: boolean = false;
  @Output() cancel = new EventEmitter<{ originalEvent: Event }>();
  @Output() successSubmit = new EventEmitter<Creator>();
  public showFormErrors: boolean = false;
  public creatorForm: FormGroup;
  public calendarOptionsFrom: FlatpickrOptions;
  public validationMessages = validationMessages;
  public checkpointCreating: boolean = false;
  public recomendationCreatiing: boolean = false;
  public showKeys: boolean = false;

  public imageOptions = {
    size: { w: 200, h: 200 },
    crop: { aspectRatio: 1 }
  };
  public videoOptions = {
    size: { w: 200, h: 200 }
  }

  public mapCenter = null;
  public markerCenter = null;

  private destroyed$ = new Subject<void>();
  public uploadTempModel: any;
  public uploadVideoModel: any;

  private fieldsRequirement = {
    id: true,
    status: true,
    name: true,
    bibliography: true,
    conclusion: true,
    main: false,
    introduction: true,
    annotation: true,
    keys: false,
    autors: false
  };


  constructor(private fb: FormBuilder,
    private creatorFormBuilderService: CreatorFormBuilderService) {
  }

  ngOnInit() {
    this.creatorForm = this.creatorFormBuilderService.buildForm(this.creator);
    console.log(this.creatorForm.get('name'));

  }
  public change(obj) {
    obj.showKeys = !obj.showKeys;
    var elems = document.getElementById('toggle');
    var elems1 = document.getElementById('toggle1');
    debugger;
    if (obj.showKeys)
    {    
      elems.style.display = 'none';
      elems1.style.display = 'none';
    }
    else{
      elems.style.display = 'block';
      elems1.style.display = 'block';
    }
  }

  // public onCoverUpload(cover: UploadImage): void {
  //   const control = this.creatorForm.get('mainImage');
  //   control.setValue([...control.value, cover]);
  //   this.uploadTempModel = null;
  // }

  // public onDeleteClicked($event, index: number): void {
  //   $event.preventDefault();
  //   const control = this.creatorForm.get('mainImage');
  //   const newValue = removeFromArray(control.value, index);
  //   control.setValue(newValue);
  // }

  // public onQuestUpload(photo: UploadImage): void {
  //   const control = this.questsForm.get('mainImage');
  //   control.setValue(photo);
  //   this.uploadTempModel = null;
  // }


  public onCancelClicked($event): void {
    $event.preventDefault();
    this.cancel.emit({ originalEvent: $event });
  }

  public onSubmit($event): void {
    $event.preventDefault();
    this.sendSubmitEvent();
  }

  private getAutorsFormArray() {
    return this.creatorForm.controls['autors'] as FormArray;
  }

  public getAutorsControl(index, control) {
    return this.getAutorsFormArray().at(index).get(control) as FormControl;
  }

  public deleteAutor(index: number) {
    this.baseDelete(index, this.getAutorsFormArray());
  }

  public addAutor() {
    const control = this.creatorForm.controls['autors'] as FormArray;
    control.push(this.initAutor());
  }

  private getMainFormArray() {
    return this.creatorForm.controls['main'] as FormArray;
  }

  public getMainControl(index, control) {
    return this.getMainFormArray().at(index).get(control) as FormControl;
  }

  public deleteMain(index: number) {
    this.baseDelete(index, this.getMainFormArray());
  }

  public addMain() {
    const control = this.creatorForm.controls['main'] as FormArray;
    control.push(this.initMain());
  }


  deleteInventory(cPointsIndex, invIndex) {
    const array = ((this.creatorForm.controls['checkpoints'] as FormArray)
      .at(cPointsIndex).get('inventory') as FormArray);
    this.baseDelete(invIndex, array);
  }

  private initAutor() {
    return this.fb.group({
      id: null,
      fullname: '',
      email: '',
      photoUpload: '',
      description: '',
      descriptionEng: '',
      work: ''
    })
  }

  private initMain() {
    return this.fb.group({
      id: null,
      name: '',
      text: ''
    })
  }

  addInventory(cPointsIndex) {
    const array = ((this.creatorForm.controls['checkpoints'] as FormArray)
      .at(cPointsIndex).get('inventory') as FormArray);
    this.baseAdd(array);
  }

  private baseAdd(formArray: FormArray, validator?: ValidatorFn | ValidatorFn[]) {
    if (validator) {
      formArray.push(this.fb.control('', validator));
    } else {
      formArray.push(this.fb.control(''));
    }
  }



  private baseDelete(index: number, formArray: FormArray) {
    if (formArray.length > 1) {
      formArray.removeAt(index);
    }
  }

  private sendSubmitEvent() {
    if (this.creatorForm.valid) {
      const creator = Object.assign({}, this.creator, this.creatorForm.value, {
      });
      this.successSubmit.emit(creator);
    } else {
      this.showFormErrors = true;
    }
  }

  public isRequired(fieldName): boolean {
    return this.fieldsRequirement[fieldName];
  }

  // public onMarkerDragEnd($event): void {
  //   this.setMarkerCoordinates($event);
  // }

  // public onMapClicked($event): void {
  //   this.setMarkerCoordinates($event);
  // }

  // private setMarkerCoordinates({ lat, lng }): void {
  //   this.mapCenter = { lat, lng };
  //   this.markerCenter = Object.assign({}, this.mapCenter);
  //   this.commitCoordinates(this.markerCenter);
  // }

  // private commitCoordinates(coords): void {
  //   this.creatorForm.get('longitude').setValue(coords.lng);
  //   this.creatorForm.get('latitude').setValue(coords.lat);
  // }

  // public onVideoUpload(video: UploadVideo, iControl: number): void {
  //   const control = ((this.questsForm.controls['checkpoints'] as FormArray)
  //     .at(iControl).get('videoUpload') as FormControl);
  //   control.setValue(video);
  //   this.uploadVideoModel = null;
  // }

  public getById(index, control) {
    const contr = ((this.creatorForm.controls['checkpoints'] as FormArray)
      .at(index).get(control) as FormControl);
    return contr;
  }

  // public deleteVideo($event, index: number) {
  //   $event.preventDefault();
  //   const control = ((this.questsForm.controls['checkpoints'] as FormArray)
  //     .at(index).get('videoUpload') as FormControl);
  //   control.setValue('');
  // }

  public onPhotoUpload(cover: UploadImage, iControl: number): void {
    const control = ((this.creatorForm.controls['autors'] as FormArray)
      .at(iControl).get('photoUpload') as FormControl);
    control.setValue(cover);
    this.uploadTempModel = null;
  }

  public onPhotoDelete($event, index: number): void {
    $event.preventDefault();
    const control = ((this.creatorForm.controls['autors'] as FormArray)
      .at(index).get('photoUpload') as FormControl);
    control.setValue('');
  }

}
