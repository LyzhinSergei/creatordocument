import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { Creator } from "../../../domain/interfaces/Creator";

@Component({
  selector: 'creator-components-creator-actions',
  templateUrl: './template.html'
})
export class CreatorComponentsCreatorActions implements OnChanges {
  @Input() creator: Creator;
  @Input() status: string;
  @Output() action = new EventEmitter<string>();

  public possibleActions = [];

  ngOnChanges(changes: SimpleChanges) {
    if (changes['creator']) {
      this.setupPossibleActions();
    }
  }

  public onCreatorAction($event): void {
    this.action.emit($event.item.value);
  }

  private setupPossibleActions() {
    this.possibleActions = [
      { label: 'Редактировать', type: 'default', value: 'edit' },
    ];
    this.possibleActions.push({ label: 'Удалить', type: 'danger', value: 'delete' });
  }
}
