﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.EntitiesConfigurations
{
    class AutorEntitiesConfiguration : IEntityTypeConfiguration<CreatorAutor>
    {
        public void Configure(EntityTypeBuilder<CreatorAutor> builder)
        {
            builder.HasOne<Creator>()
              .WithMany()
              .HasForeignKey(x => x.CreatortId).IsRequired();
        }
    }
}
