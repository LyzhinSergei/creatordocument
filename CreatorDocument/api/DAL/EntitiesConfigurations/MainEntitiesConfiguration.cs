﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.EntitiesConfigurations
{
    class MainEntitiesConfiguration : IEntityTypeConfiguration<CreatorMain>
    {
        public void Configure(EntityTypeBuilder<CreatorMain> builder)
        {
            builder.HasOne<Creator>()
              .WithMany()
              .HasForeignKey(x => x.CreatorId).IsRequired();
        }
    }
}
