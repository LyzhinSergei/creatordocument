﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class addEngVersion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AnnotationEng",
                table: "Creator",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "KeysEng",
                table: "Creator",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Autors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DescriptionEng",
                table: "Autors",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AnnotationEng",
                table: "Creator");

            migrationBuilder.DropColumn(
                name: "KeysEng",
                table: "Creator");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Autors");

            migrationBuilder.DropColumn(
                name: "DescriptionEng",
                table: "Autors");
        }
    }
}
