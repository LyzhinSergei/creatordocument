﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DAL.Migrations
{
    public partial class creator : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CheckPoints_Quests_QuestId",
                table: "CheckPoints");

            migrationBuilder.DropForeignKey(
                name: "FK_StocksQuests_Quests_QuestId",
                table: "StocksQuests");

            migrationBuilder.DropTable(
                name: "QuestRecomendations");

            migrationBuilder.DropTable(
                name: "Quests");

            migrationBuilder.DropIndex(
                name: "IX_CheckPoints_QuestId",
                table: "CheckPoints");

            migrationBuilder.DropColumn(
                name: "QuestId",
                table: "CheckPoints");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "News",
                maxLength: 100000,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 1000);

            migrationBuilder.CreateTable(
                name: "Creator",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true),
                    Status = table.Column<string>(maxLength: 10, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Bibliography = table.Column<string>(nullable: true),
                    Conclusion = table.Column<string>(nullable: true),
                    Introduction = table.Column<string>(nullable: true),
                    Annotation = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Creator", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Autors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Work = table.Column<string>(nullable: true),
                    CreatorId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Autors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Autors_Creator_CreatorId",
                        column: x => x.CreatorId,
                        principalTable: "Creator",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CreatorAutors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Work = table.Column<string>(nullable: true),
                    CreatortId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CreatorAutors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CreatorAutors_Creator_CreatortId",
                        column: x => x.CreatortId,
                        principalTable: "Creator",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CreatorKeys",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Key = table.Column<string>(nullable: true),
                    CreatorId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CreatorKeys", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CreatorKeys_Creator_CreatorId",
                        column: x => x.CreatorId,
                        principalTable: "Creator",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CreatorMains",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true),
                    CreatorId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CreatorMains", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CreatorMains_Creator_CreatorId",
                        column: x => x.CreatorId,
                        principalTable: "Creator",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Keys",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    KeyName = table.Column<string>(nullable: true),
                    CreatorId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Keys", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Keys_Creator_CreatorId",
                        column: x => x.CreatorId,
                        principalTable: "Creator",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Mains",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true),
                    CreatorId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mains", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Mains_Creator_CreatorId",
                        column: x => x.CreatorId,
                        principalTable: "Creator",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Autors_CreatorId",
                table: "Autors",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_CreatorAutors_CreatortId",
                table: "CreatorAutors",
                column: "CreatortId");

            migrationBuilder.CreateIndex(
                name: "IX_CreatorKeys_CreatorId",
                table: "CreatorKeys",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_CreatorMains_CreatorId",
                table: "CreatorMains",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Keys_CreatorId",
                table: "Keys",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Mains_CreatorId",
                table: "Mains",
                column: "CreatorId");

            migrationBuilder.AddForeignKey(
                name: "FK_StocksQuests_Creator_QuestId",
                table: "StocksQuests",
                column: "QuestId",
                principalTable: "Creator",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StocksQuests_Creator_QuestId",
                table: "StocksQuests");

            migrationBuilder.DropTable(
                name: "Autors");

            migrationBuilder.DropTable(
                name: "CreatorAutors");

            migrationBuilder.DropTable(
                name: "CreatorKeys");

            migrationBuilder.DropTable(
                name: "CreatorMains");

            migrationBuilder.DropTable(
                name: "Keys");

            migrationBuilder.DropTable(
                name: "Mains");

            migrationBuilder.DropTable(
                name: "Creator");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "News",
                maxLength: 1000,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 100000);

            migrationBuilder.AddColumn<int>(
                name: "QuestId",
                table: "CheckPoints",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "QuestRecomendations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Inventory = table.Column<string>(nullable: true),
                    PhotoUploadId = table.Column<string>(nullable: true),
                    QuestId = table.Column<int>(nullable: false),
                    TimeSpend = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuestRecomendations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Quests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Address = table.Column<string>(nullable: true),
                    Avaliability = table.Column<string>(nullable: true),
                    Cost = table.Column<decimal>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(maxLength: 5000, nullable: true),
                    MainImage = table.Column<string[]>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    PlayersCount = table.Column<int>(nullable: false),
                    Status = table.Column<string>(maxLength: 10, nullable: false),
                    StockId = table.Column<int>(nullable: true),
                    TimeToFinish = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Quests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Quests_Stocks_StockId",
                        column: x => x.StockId,
                        principalTable: "Stocks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CheckPoints_QuestId",
                table: "CheckPoints",
                column: "QuestId");

            migrationBuilder.CreateIndex(
                name: "IX_Quests_StockId",
                table: "Quests",
                column: "StockId");

            migrationBuilder.AddForeignKey(
                name: "FK_CheckPoints_Quests_QuestId",
                table: "CheckPoints",
                column: "QuestId",
                principalTable: "Quests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_StocksQuests_Quests_QuestId",
                table: "StocksQuests",
                column: "QuestId",
                principalTable: "Quests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
