﻿// <auto-generated />
using System;
using DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DAL.Migrations
{
    [DbContext(typeof(EfContext))]
    [Migration("20200521204901_addEngVersion")]
    partial class addEngVersion
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "2.1.14-servicing-32113")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("DAL.Entities.Autor", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CreatorId");

                    b.Property<string>("Description");

                    b.Property<string>("DescriptionEng");

                    b.Property<string>("Email");

                    b.Property<string>("Name");

                    b.Property<string>("PhotoId");

                    b.Property<string>("Work");

                    b.HasKey("Id");

                    b.HasIndex("CreatorId");

                    b.ToTable("Autors");
                });

            modelBuilder.Entity("DAL.Entities.Box", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Avaliability");

                    b.Property<int>("CheckPointsId");

                    b.Property<int>("CheckpointsCount");

                    b.Property<decimal>("Cost");

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<string>("Description")
                        .HasMaxLength(5000);

                    b.Property<string[]>("MainImage");

                    b.Property<string>("Name");

                    b.Property<int>("PlayersCount");

                    b.Property<string>("Status")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<int?>("StockId");

                    b.Property<string>("TimeToFinish");

                    b.Property<string>("Type");

                    b.HasKey("Id");

                    b.HasIndex("StockId");

                    b.ToTable("Boxes");
                });

            modelBuilder.Entity("DAL.Entities.BoxCheckpoint", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("BoxId");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("BoxId");

                    b.ToTable("BoxCheckpoint");
                });

            modelBuilder.Entity("DAL.Entities.CheckPoint", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("BoxId");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longtitude");

                    b.Property<string>("PhotoId");

                    b.Property<string>("VideoId");

                    b.HasKey("Id");

                    b.HasIndex("BoxId");

                    b.ToTable("CheckPoints");
                });

            modelBuilder.Entity("DAL.Entities.Creator", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Annotation");

                    b.Property<string>("AnnotationEng");

                    b.Property<string>("Bibliography");

                    b.Property<string>("Conclusion");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("Introduction");

                    b.Property<string>("Keys");

                    b.Property<string>("KeysEng");

                    b.Property<string>("Name");

                    b.Property<string>("Status")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<string>("Type");

                    b.HasKey("Id");

                    b.ToTable("Creator");
                });

            modelBuilder.Entity("DAL.Entities.CreatorAutor", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreatortId");

                    b.Property<string>("Email");

                    b.Property<string>("Name");

                    b.Property<string>("Work");

                    b.HasKey("Id");

                    b.HasIndex("CreatortId");

                    b.ToTable("CreatorAutors");
                });

            modelBuilder.Entity("DAL.Entities.CreatorMain", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreatorId");

                    b.Property<string>("Name");

                    b.Property<string>("Text");

                    b.HasKey("Id");

                    b.HasIndex("CreatorId");

                    b.ToTable("CreatorMains");
                });

            modelBuilder.Entity("DAL.Entities.FAQ", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Answer")
                        .IsRequired()
                        .HasMaxLength(1000);

                    b.Property<DateTime>("CreateDate");

                    b.Property<string>("Question")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("Status")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("FAQs");
                });

            modelBuilder.Entity("DAL.Entities.Inventory", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("BoxId");

                    b.Property<int?>("CheckPointId");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("BoxId");

                    b.HasIndex("CheckPointId");

                    b.ToTable("Inventories");
                });

            modelBuilder.Entity("DAL.Entities.Login", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Hash");

                    b.Property<string>("Status");

                    b.Property<string>("Type");

                    b.Property<string>("UserName");

                    b.HasKey("Id");

                    b.ToTable("Logins");
                });

            modelBuilder.Entity("DAL.Entities.Main", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CreatorId");

                    b.Property<string>("Name");

                    b.Property<string>("Text");

                    b.HasKey("Id");

                    b.HasIndex("CreatorId");

                    b.ToTable("Mains");
                });

            modelBuilder.Entity("DAL.Entities.NewsItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(100000);

                    b.Property<string>("Status")
                        .IsRequired();

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.HasKey("Id");

                    b.ToTable("News");
                });

            modelBuilder.Entity("DAL.Entities.NewsUploads", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("NewsId");

                    b.Property<string>("PhotoUploadId");

                    b.Property<string>("VideoUploadId");

                    b.HasKey("Id");

                    b.ToTable("NewsUploads");
                });

            modelBuilder.Entity("DAL.Entities.Payments", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("City");

                    b.Property<int>("Client");

                    b.Property<string>("Comment");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("DelivelerTime");

                    b.Property<int>("ProductId");

                    b.Property<string>("State");

                    b.Property<string>("Status");

                    b.Property<double>("Sum");

                    b.Property<string>("Type");

                    b.HasKey("Id");

                    b.ToTable("Payments");
                });

            modelBuilder.Entity("DAL.Entities.Presents", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<byte>("Age");

                    b.Property<string>("City");

                    b.Property<string>("Commentary");

                    b.Property<string>("Email");

                    b.Property<string>("FullName");

                    b.Property<string>("Phone");

                    b.Property<int>("ProductId");

                    b.Property<bool>("ToAnotherCity");

                    b.Property<string>("Type");

                    b.Property<int>("UserBy");

                    b.HasKey("Id");

                    b.ToTable("Presents");
                });

            modelBuilder.Entity("DAL.Entities.ProductHelper", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<string>("PhotoUploadId");

                    b.Property<int>("RequestId");

                    b.HasKey("Id");

                    b.HasIndex("RequestId");

                    b.ToTable("ProductHelpers");
                });

            modelBuilder.Entity("DAL.Entities.PromoCode", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Comment")
                        .HasMaxLength(200);

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime>("End");

                    b.Property<string>("Name");

                    b.Property<double>("Percent");

                    b.Property<string>("Status")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("PromoCodes");
                });

            modelBuilder.Entity("DAL.Entities.Request", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateDate");

                    b.Property<string>("Description");

                    b.Property<string>("Email");

                    b.Property<string>("Location");

                    b.Property<string>("Phone");

                    b.Property<decimal>("Price");

                    b.Property<string>("ProductName")
                        .IsRequired();

                    b.Property<DateTime>("StartDate");

                    b.Property<string>("Status")
                        .IsRequired();

                    b.Property<string>("UserName")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Requests");
                });

            modelBuilder.Entity("DAL.Entities.Review", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Comment")
                        .HasMaxLength(500);

                    b.Property<DateTime>("CreatedDate");

                    b.Property<int>("ProductId");

                    b.Property<int>("Rating");

                    b.Property<string>("Status");

                    b.Property<string>("TypeProduct");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Reviews");
                });

            modelBuilder.Entity("DAL.Entities.Stock", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AmountPersent");

                    b.Property<DateTime>("Begin");

                    b.Property<string>("Description");

                    b.Property<DateTime>("End");

                    b.Property<string>("PhotoUploadId");

                    b.Property<string>("Status");

                    b.Property<string>("Title");

                    b.HasKey("Id");

                    b.ToTable("Stocks");
                });

            modelBuilder.Entity("DAL.Entities.StocksBoxes", b =>
                {
                    b.Property<int>("StockId");

                    b.Property<int>("BoxId");

                    b.HasKey("StockId", "BoxId");

                    b.HasIndex("BoxId");

                    b.ToTable("StocksBoxes");
                });

            modelBuilder.Entity("DAL.Entities.StocksQuests", b =>
                {
                    b.Property<int>("StockId");

                    b.Property<int>("QuestId");

                    b.HasKey("StockId", "QuestId");

                    b.HasIndex("QuestId");

                    b.ToTable("StocksQuests");
                });

            modelBuilder.Entity("DAL.Entities.Support", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AskedAt");

                    b.Property<string>("Email");

                    b.Property<string>("Message");

                    b.Property<string>("Problem");

                    b.HasKey("Id");

                    b.ToTable("Supports");
                });

            modelBuilder.Entity("DAL.Entities.UniqGuest", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Date");

                    b.Property<string>("IpAdress");

                    b.HasKey("Id");

                    b.ToTable("UniqGuests");
                });

            modelBuilder.Entity("DAL.Entities.Upload", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(36);

                    b.Property<int>("Bitrate");

                    b.Property<string>("ContentType");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<double>("Duration");

                    b.Property<string>("Extension");

                    b.Property<int>("Height");

                    b.Property<int>("SizeInBytes");

                    b.Property<string>("Type");

                    b.Property<int>("Width");

                    b.HasKey("Id");

                    b.ToTable("Uploads");
                });

            modelBuilder.Entity("DAL.Entities.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("BirthDate");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("Email");

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName")
                        .HasMaxLength(100);

                    b.Property<string>("LastName")
                        .HasMaxLength(100);

                    b.Property<int?>("LoginId");

                    b.Property<string>("OperationsToken");

                    b.Property<string>("PatronymicName")
                        .HasMaxLength(100);

                    b.Property<string>("Phone")
                        .HasMaxLength(50);

                    b.Property<string>("PhotoUploadId")
                        .HasMaxLength(36);

                    b.Property<string>("Status")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<string>("UserName");

                    b.HasKey("Id");

                    b.HasIndex("LoginId");

                    b.HasIndex("PhotoUploadId");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("DAL.Entities.UserPurchases", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("BoughtDate");

                    b.Property<bool>("IsBox");

                    b.Property<bool>("IsDone");

                    b.Property<bool>("IsQuest");

                    b.Property<int>("ProductId");

                    b.Property<string>("Review");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.ToTable("UserPurchaseses");
                });

            modelBuilder.Entity("DAL.Entities.VideosItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(1000);

                    b.Property<string>("Status")
                        .IsRequired();

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.HasKey("Id");

                    b.ToTable("Videos");
                });

            modelBuilder.Entity("DAL.Entities.VideosUpload", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("VideoId");

                    b.Property<string>("VideoUploadId");

                    b.HasKey("Id");

                    b.ToTable("VideosUpload");
                });

            modelBuilder.Entity("DAL.Entities.Autor", b =>
                {
                    b.HasOne("DAL.Entities.Creator")
                        .WithMany("Autors")
                        .HasForeignKey("CreatorId");
                });

            modelBuilder.Entity("DAL.Entities.Box", b =>
                {
                    b.HasOne("DAL.Entities.Stock")
                        .WithMany()
                        .HasForeignKey("StockId");
                });

            modelBuilder.Entity("DAL.Entities.BoxCheckpoint", b =>
                {
                    b.HasOne("DAL.Entities.Box")
                        .WithMany()
                        .HasForeignKey("BoxId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DAL.Entities.CheckPoint", b =>
                {
                    b.HasOne("DAL.Entities.Box")
                        .WithMany("CheckPoints")
                        .HasForeignKey("BoxId");
                });

            modelBuilder.Entity("DAL.Entities.CreatorAutor", b =>
                {
                    b.HasOne("DAL.Entities.Creator")
                        .WithMany()
                        .HasForeignKey("CreatortId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DAL.Entities.CreatorMain", b =>
                {
                    b.HasOne("DAL.Entities.Creator")
                        .WithMany()
                        .HasForeignKey("CreatorId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DAL.Entities.Inventory", b =>
                {
                    b.HasOne("DAL.Entities.Box")
                        .WithMany()
                        .HasForeignKey("BoxId");

                    b.HasOne("DAL.Entities.CheckPoint")
                        .WithMany("Inventories")
                        .HasForeignKey("CheckPointId");
                });

            modelBuilder.Entity("DAL.Entities.Main", b =>
                {
                    b.HasOne("DAL.Entities.Creator")
                        .WithMany("Main")
                        .HasForeignKey("CreatorId");
                });

            modelBuilder.Entity("DAL.Entities.ProductHelper", b =>
                {
                    b.HasOne("DAL.Entities.Request")
                        .WithMany("ProductHelper")
                        .HasForeignKey("RequestId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DAL.Entities.Review", b =>
                {
                    b.HasOne("DAL.Entities.User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DAL.Entities.StocksBoxes", b =>
                {
                    b.HasOne("DAL.Entities.Box")
                        .WithMany()
                        .HasForeignKey("BoxId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("DAL.Entities.Stock")
                        .WithMany()
                        .HasForeignKey("StockId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DAL.Entities.StocksQuests", b =>
                {
                    b.HasOne("DAL.Entities.Creator")
                        .WithMany()
                        .HasForeignKey("QuestId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("DAL.Entities.Stock")
                        .WithMany()
                        .HasForeignKey("StockId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DAL.Entities.User", b =>
                {
                    b.HasOne("DAL.Entities.Login")
                        .WithMany()
                        .HasForeignKey("LoginId");

                    b.HasOne("DAL.Entities.Upload")
                        .WithMany()
                        .HasForeignKey("PhotoUploadId");
                });
#pragma warning restore 612, 618
        }
    }
}
