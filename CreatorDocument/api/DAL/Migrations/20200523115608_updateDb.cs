﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class updateDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CreatorAutors_Creator_CreatortId",
                table: "CreatorAutors");

            migrationBuilder.DropForeignKey(
                name: "FK_CreatorMains_Creator_CreatorId",
                table: "CreatorMains");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CreatorMains",
                table: "CreatorMains");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CreatorAutors",
                table: "CreatorAutors");

            migrationBuilder.RenameTable(
                name: "CreatorMains",
                newName: "CreatorMain");

            migrationBuilder.RenameTable(
                name: "CreatorAutors",
                newName: "CreatorAutor");

            migrationBuilder.RenameIndex(
                name: "IX_CreatorMains_CreatorId",
                table: "CreatorMain",
                newName: "IX_CreatorMain_CreatorId");

            migrationBuilder.RenameIndex(
                name: "IX_CreatorAutors_CreatortId",
                table: "CreatorAutor",
                newName: "IX_CreatorAutor_CreatortId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CreatorMain",
                table: "CreatorMain",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CreatorAutor",
                table: "CreatorAutor",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CreatorAutor_Creator_CreatortId",
                table: "CreatorAutor",
                column: "CreatortId",
                principalTable: "Creator",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CreatorMain_Creator_CreatorId",
                table: "CreatorMain",
                column: "CreatorId",
                principalTable: "Creator",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CreatorAutor_Creator_CreatortId",
                table: "CreatorAutor");

            migrationBuilder.DropForeignKey(
                name: "FK_CreatorMain_Creator_CreatorId",
                table: "CreatorMain");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CreatorMain",
                table: "CreatorMain");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CreatorAutor",
                table: "CreatorAutor");

            migrationBuilder.RenameTable(
                name: "CreatorMain",
                newName: "CreatorMains");

            migrationBuilder.RenameTable(
                name: "CreatorAutor",
                newName: "CreatorAutors");

            migrationBuilder.RenameIndex(
                name: "IX_CreatorMain_CreatorId",
                table: "CreatorMains",
                newName: "IX_CreatorMains_CreatorId");

            migrationBuilder.RenameIndex(
                name: "IX_CreatorAutor_CreatortId",
                table: "CreatorAutors",
                newName: "IX_CreatorAutors_CreatortId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CreatorMains",
                table: "CreatorMains",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CreatorAutors",
                table: "CreatorAutors",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CreatorAutors_Creator_CreatortId",
                table: "CreatorAutors",
                column: "CreatortId",
                principalTable: "Creator",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CreatorMains_Creator_CreatorId",
                table: "CreatorMains",
                column: "CreatorId",
                principalTable: "Creator",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
