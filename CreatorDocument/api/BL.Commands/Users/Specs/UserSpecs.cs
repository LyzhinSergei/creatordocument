﻿using System.Threading.Tasks;
using DAL;
using Interfaces;
using Interfaces.Specs;
using Microsoft.EntityFrameworkCore;

namespace BL.Commands.Users.Specs
{
    public class UserSpecs : IUserSpecs
    {
        private readonly EfContext _db;

        public UserSpecs(EfContext db)
        {
            _db = db;
        }

     
        public async Task<SpecResult> IsUserEmailUnique(string email)
        {
            if (await _db.Users.AnyAsync(x => x.Email == email))
            {
                return SpecResult.No("EMAIL_NOT_UNIQUE", "Email пользователя не уникален");
            }

            return SpecResult.Yes();
        }
    }
}
