﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DAL.Entities;
using Interfaces;
using Interfaces.Contexts;
using Microsoft.EntityFrameworkCore;

namespace BL.Commands.Quests
{
    public class CreatorUpdateCommand : ICommand<CreatorUpdateContext>
    {
        private readonly EfContext _db;

        public CreatorUpdateCommand(EfContext db) => _db = db;

        public async Task<CommandResult> ExecuteAsync(CreatorUpdateContext context)
        {
            var creator = await _db.Creator
                .Include(x=>x.Main)
                .Include(x=>x.Autors)
                .FirstOrDefaultAsync(x => x.Id == context.Id);

            if (creator == null) return CommandResult.Fail("DOC_NOT_FOUND", "Документ не найден");
            
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                creator.Annotation = context.Creator.Annotation;
                creator.Bibliography = context.Creator.Bibliography;
                creator.Conclusion = context.Creator.Conclusion;
                creator.CreatedDate = context.Creator.CreatedDate;
                creator.Introduction = context.Creator.Introduction;
                creator.Name = context.Creator.Name;
                creator.Status = context.Creator.Status;
                creator.Keys = context.Creator.Keys;
                creator.AnnotationEng = context.Creator.AnnotationEng;
                creator.KeysEng = context.Creator.KeysEng;
                creator.Link = context.Creator.Link;

                creator.Status = DAL.Entities.Creator.Statuses.Active;

                await _db.SaveChangesAsync();

                transaction.Commit();
            }
            return CommandResult.Success();
        }

        private async Task UpdateMain(Creator model, int id)
        {
            if (model != null)
            {
                var creatorMain = await _db.CreatorMain
                                         .Where(x => x.CreatorId == id)
                                         .ToListAsync();

                _db.CreatorMain.RemoveRange(creatorMain);

                await _db.SaveChangesAsync();

                foreach (var item in model.Main)
                {
                    _db.CreatorMain.Add(new DAL.Entities.CreatorMain
                    {
                        CreatorId = id,
                        Id = item.Id,
                        Name = item.Name,
                        Text = item.Text
                    });

                }
                await _db.SaveChangesAsync();
            }
        }
    }
}