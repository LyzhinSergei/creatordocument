﻿using DAL;
using Interfaces;
using Interfaces.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BL.Commands.Quests
{
    public class CreatorDeleteCommand : ICommand<CreatorDeleteContext>
    {
        private readonly EfContext _db;
        
        public CreatorDeleteCommand(EfContext db) => _db = db;

        public async Task<CommandResult> ExecuteAsync(CreatorDeleteContext context)
        {
            var creator = await _db.Creator.FirstOrDefaultAsync(x => x.Id == context.Id);
            if (creator == null) return CommandResult.Fail("DOC_NOT_FOUND", "Документ не найден");
            creator.Status = DAL.Entities.Creator.Statuses.Deleted;
            await _db.SaveChangesAsync();
            return CommandResult.Success();

        }
    }
}