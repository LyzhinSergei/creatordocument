﻿using DAL;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Interfaces.Contexts;
using Interfaces.Specs;
using System.Linq;
using DAL.Entities;

namespace BL.Commands.Quests
{
    public class CreatorCreateCommand : ICommand<CreatorCreateContext>
    {
        private readonly EfContext _db;

        public CreatorCreateCommand(EfContext db) => _db = db;

        public async Task<CommandResult> ExecuteAsync(CreatorCreateContext context)
        {
            using (var transaction = await _db.Database.BeginTransactionAsync())
            {
                context.Creator.Type = "DOC";
                context.Creator.Status = DAL.Entities.Creator.Statuses.Active;
                _db.Autors.AddRange(context.Creator.Autors);
                _db.Mains.AddRange(context.Creator.Main);
                await _db.SaveChangesAsync();
                _db.Creator.Add(context.Creator);

                await _db.SaveChangesAsync();
                transaction.Commit();
            }

            return CommandResult.Success();
        }
    }
}