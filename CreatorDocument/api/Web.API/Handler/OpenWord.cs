﻿using W = DocumentFormat.OpenXml.Wordprocessing;
using M = DocumentFormat.OpenXml.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml.Packaging;
using System.IO;
using DocumentFormat.OpenXml;
using DAL.Entities;
using DocumentFormat.OpenXml.Presentation;
using DocumentFormat.OpenXml.Office.CustomUI;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Diagnostics.Contracts;
using Web.API.Helper;
using System.Net;

namespace OMath
{
    public class OpenWord
    {
        public WordprocessingDocument wDoc;
        public MainDocumentPart mainDoc;
        public W.Body body;
        public W.Document doc;

        public void Init(string fileName)
        {
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
            wDoc = WordprocessingDocument.Create(fileName, WordprocessingDocumentType.Document);
            mainDoc = wDoc.AddMainDocumentPart();
            doc = new W.Document();
            body = new W.Body();
        }

        public void CreateAutorsDescription(List<Autor> Autors)
        {
            CreateText("Calibri", "28", true, false, "СВЕДЕНИЯ ОБ АВТОРАХ", true, 0, JustificationValues.Left);

            Autors.ForEach(value =>
            {
                var link = $@"https://327517.selcdn.ru/darkxx/uploads/" + value.PhotoId + value.Extension;
                var splitName = value.Name.Split(' ');
                string name = $"{splitName[0].ToUpper()} {splitName[1]} {splitName[2]}";
                InsertAPicture(link, value.PhotoId);
                CreateText("Calibri", "24", true, true, name + " - ", true, 0, JustificationValues.Both, "Calibri", "24", false, false, value.Description);
                CreateText("Calibri", "24", true, true, TranslateHelper.Translite(name).ToUpper() + " - ", true, 0, JustificationValues.Both, "Calibri", "24", false, false, value.DescriptionEng);
                CreateText("Calibri", "24", false, false, "email" + ": ", true, 0, JustificationValues.Both, "Calibri", "24", false, false, value.Email);
            });
        }

        public void InsertAPicture(string uri, string tag)
        {
            ImagePart imagePart = mainDoc.AddImagePart(ImagePartType.Jpeg);
            byte[] imageData = DownloadData(uri);
            MemoryStream stream = new MemoryStream(imageData);
            imagePart.FeedData(stream);
            stream.Close();

            PicHelper.AddImageToBody(body, mainDoc.GetIdOfPart(imagePart));
        }

        private byte[] DownloadData(string url)
        {
            var downloadedData = new byte[0];

            WebRequest req = WebRequest.Create(url);
            WebResponse response = req.GetResponse();
            Stream stream = response.GetResponseStream();

            byte[] buffer = new byte[1024];
            MemoryStream memStream = new MemoryStream();
            while (true)
            {
                int bytesRead = stream.Read(buffer, 0, buffer.Length);

                if (bytesRead == 0)
                {
                    break;
                }
                else
                {
                    memStream.Write(buffer, 0, bytesRead);
                }
            }
            downloadedData = memStream.ToArray();
            stream.Close();
            memStream.Close();

            return downloadedData;
        }

        internal void CreateLine()
        {
            W.Paragraph para = body.AppendChild(new W.Paragraph());
            W.Run run = para.AppendChild(new W.Run());
            W.RunProperties runProperties = run.AppendChild(new W.RunProperties());

            Bold bold = new Bold() { Val = true };
            RunFonts runFont = new RunFonts() { Ascii = "Calibri", HighAnsi = "Calibri" };
            FontSize fonSize = new FontSize() { Val = "28" };
            W.Text text1 = new W.Text() { Text = "_____________________________________________________________________", Space = SpaceProcessingModeValues.Preserve };
            runProperties.AppendChild(runFont);
            runProperties.AppendChild(fonSize);
            runProperties.AppendChild(bold);

            run.Append(text1);
        } 

        internal void CreateBibliography(string bibliography, bool onEnglish)
        {
            CreateText("Calibri", "28", true, false, onEnglish ? "REFERENCES" : "СПИСОК ЛИТЕРАТУРЫ", true, 0, JustificationValues.Left);
            var textSplit = bibliography.Split('\n').ToList();
            int i = 1;
            textSplit.ForEach(value =>
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    CreateText("Calibri", "28", false, false, $"{i}. {value}", true, 0, JustificationValues.Both);
                    i++;
                }
            });
        }

        internal void CreateConclusion(string conclusion)
        {
            CreateText("Calibri", "28", true, false, "ЗАКЛЮЧЕНИЕ", true, 0, JustificationValues.Left);
            var textSplit = conclusion.Split('\n').ToList();
            textSplit.ForEach(value =>
            {
                if (!string.IsNullOrWhiteSpace(value))
                    CreateText("Calibri", "28", false, false, value, true, 0, JustificationValues.Both);
            });
        }

        internal void CreateMain(List<Main> main)
        {
            main.ForEach(value =>
            {
                CreateText("Calibri", "28", true, false, value.Name, true, 0, JustificationValues.Left);

                var textSplit = value.Text.Split('\n').ToList();
                textSplit.ForEach(text =>
                {
                    if (!string.IsNullOrWhiteSpace(text))
                        CreateText("Calibri", "28", false, false, text, true, 0, JustificationValues.Both);
                });
            });
        }

        internal void CreateIntroduction(string introduction)
        {
            CreateText("Calibri", "28", true, false, "ВВЕДЕНИЕ", true, 0, JustificationValues.Left);
            var textSplit = introduction.Split('\n').ToList();
            textSplit.ForEach(value =>
            {
                if (!string.IsNullOrWhiteSpace(value))
                    CreateText("Calibri", "28", false, false, value, true, 0, JustificationValues.Both);
            });
        }

        public void CreateKeyWords(string keys, bool onEnglish)
        {
            CreateText("Calibri", "28", true, true, onEnglish ? "Keywords: " : "Ключевые слова: ", true, 0, JustificationValues.Both, "Calibri", "28", false, false, keys);
        }

        public void CreateAnnotation(string annotation, bool onEnglish)
        {
            CreateText("Calibri", "28", true, true, onEnglish ? "Abstract" :"Аннотация", true, 0, JustificationValues.Left);
            var textSplit = annotation.Split('\n').ToList();
            textSplit.ForEach(value =>
            {
                if (!string.IsNullOrWhiteSpace(value))
                    CreateText("Calibri", "28", false, false, value, true, 0, JustificationValues.Both);
            });
        }

        public void CreateNameAutors(List<Autor> autors, bool onEnglish)
        {
            W.Paragraph paragraph1 = new W.Paragraph();
            W.Paragraph paragraph2 = new W.Paragraph();
            W.Paragraph paragraph3 = new W.Paragraph();
            /*** Format text1, text2, text3 and text4  as one paragraph ***/
            var run1 = new DocumentFormat.OpenXml.Wordprocessing.Run();
            var run2 = new DocumentFormat.OpenXml.Wordprocessing.Run();
            var run3 = new DocumentFormat.OpenXml.Wordprocessing.Run();

            int i = 1;
            autors.ForEach(value =>
            {
                var name = value.Name;
                var work = value.Work;
                string lastname = "";
                string firstname = "";
                string secondname = "";
                if (onEnglish)
                {
                    name = TranslateHelper.Translite(value.Name);
                    work = TranslateHelper.Translate(value.Work);
                }
                var splitName = name.Split(' ');
                lastname = splitName[0];
                firstname = splitName[1].Substring(0, 1);
                secondname = splitName[2].Substring(0, 1);
                name = $"{firstname}.{secondname}. {lastname}";
                DocumentFormat.OpenXml.Wordprocessing.RunProperties runProperties1 =
               new DocumentFormat.OpenXml.Wordprocessing.RunProperties
               (new RunFonts() { Ascii = "Calibri" });
                DocumentFormat.OpenXml.Wordprocessing.RunProperties runProperties2 =
                   new DocumentFormat.OpenXml.Wordprocessing.RunProperties
                   (new RunFonts() { Ascii = "Calibri" });
                DocumentFormat.OpenXml.Wordprocessing.RunProperties runProperties3 =
                   new DocumentFormat.OpenXml.Wordprocessing.RunProperties
                   (new RunFonts() { Ascii = "Calibri" });
                DocumentFormat.OpenXml.Wordprocessing.RunProperties runProperties4 =
              new DocumentFormat.OpenXml.Wordprocessing.RunProperties
              (new RunFonts() { Ascii = "Calibri" });
                DocumentFormat.OpenXml.Wordprocessing.RunProperties runProperties5 =
                   new DocumentFormat.OpenXml.Wordprocessing.RunProperties
                   (new RunFonts() { Ascii = "Calibri" });
                DocumentFormat.OpenXml.Wordprocessing.RunProperties runProperties6 =
                   new DocumentFormat.OpenXml.Wordprocessing.RunProperties
                   (new RunFonts() { Ascii = "Calibri" });
                DocumentFormat.OpenXml.Wordprocessing.RunProperties runProperties7 =
              new DocumentFormat.OpenXml.Wordprocessing.RunProperties
              (new RunFonts() { Ascii = "Calibri" });
                DocumentFormat.OpenXml.Wordprocessing.RunProperties runProperties8 =
                   new DocumentFormat.OpenXml.Wordprocessing.RunProperties
                   (new RunFonts() { Ascii = "Calibri" });
                DocumentFormat.OpenXml.Wordprocessing.RunProperties runProperties9 =
                   new DocumentFormat.OpenXml.Wordprocessing.RunProperties
                   (new RunFonts() { Ascii = "Calibri" });

                DocumentFormat.OpenXml.Wordprocessing.FontSize fs1 =
                   new DocumentFormat.OpenXml.Wordprocessing.FontSize();
                DocumentFormat.OpenXml.Wordprocessing.Color c1 =
                   new DocumentFormat.OpenXml.Wordprocessing.Color();
                DocumentFormat.OpenXml.Wordprocessing.Bold b1 =
                   new DocumentFormat.OpenXml.Wordprocessing.Bold();
                DocumentFormat.OpenXml.Wordprocessing.VerticalTextAlignment vta1 =
                   new DocumentFormat.OpenXml.Wordprocessing.VerticalTextAlignment();

                DocumentFormat.OpenXml.Wordprocessing.FontSize fs2 =
                   new DocumentFormat.OpenXml.Wordprocessing.FontSize();
                DocumentFormat.OpenXml.Wordprocessing.Color c2 =
                   new DocumentFormat.OpenXml.Wordprocessing.Color();
                DocumentFormat.OpenXml.Wordprocessing.Bold b2 =
                   new DocumentFormat.OpenXml.Wordprocessing.Bold();
                DocumentFormat.OpenXml.Wordprocessing.VerticalTextAlignment vta2 =
                   new DocumentFormat.OpenXml.Wordprocessing.VerticalTextAlignment();

                DocumentFormat.OpenXml.Wordprocessing.FontSize fs3 =
                   new DocumentFormat.OpenXml.Wordprocessing.FontSize();
                DocumentFormat.OpenXml.Wordprocessing.Color c3 =
                   new DocumentFormat.OpenXml.Wordprocessing.Color();
                DocumentFormat.OpenXml.Wordprocessing.Bold b3 =
                   new DocumentFormat.OpenXml.Wordprocessing.Bold();
                DocumentFormat.OpenXml.Wordprocessing.VerticalTextAlignment vta3 =
                   new DocumentFormat.OpenXml.Wordprocessing.VerticalTextAlignment();

                DocumentFormat.OpenXml.Wordprocessing.FontSize fs4 =
                   new DocumentFormat.OpenXml.Wordprocessing.FontSize();
                DocumentFormat.OpenXml.Wordprocessing.Color c4 =
                   new DocumentFormat.OpenXml.Wordprocessing.Color();
                DocumentFormat.OpenXml.Wordprocessing.Italic b4 =
                   new DocumentFormat.OpenXml.Wordprocessing.Italic();
                DocumentFormat.OpenXml.Wordprocessing.VerticalTextAlignment vta4 =
                   new DocumentFormat.OpenXml.Wordprocessing.VerticalTextAlignment();

                DocumentFormat.OpenXml.Wordprocessing.FontSize fs5 =
                   new DocumentFormat.OpenXml.Wordprocessing.FontSize();
                DocumentFormat.OpenXml.Wordprocessing.Color c5 =
                   new DocumentFormat.OpenXml.Wordprocessing.Color();
                DocumentFormat.OpenXml.Wordprocessing.Italic b5 =
                   new DocumentFormat.OpenXml.Wordprocessing.Italic();
                DocumentFormat.OpenXml.Wordprocessing.VerticalTextAlignment vta5 =
                   new DocumentFormat.OpenXml.Wordprocessing.VerticalTextAlignment();

                DocumentFormat.OpenXml.Wordprocessing.FontSize fs6 =
                   new DocumentFormat.OpenXml.Wordprocessing.FontSize();
                DocumentFormat.OpenXml.Wordprocessing.Color c6 =
                   new DocumentFormat.OpenXml.Wordprocessing.Color();
                DocumentFormat.OpenXml.Wordprocessing.Italic b6 =
                   new DocumentFormat.OpenXml.Wordprocessing.Italic();
                DocumentFormat.OpenXml.Wordprocessing.VerticalTextAlignment vta6 =
                   new DocumentFormat.OpenXml.Wordprocessing.VerticalTextAlignment();

                DocumentFormat.OpenXml.Wordprocessing.FontSize fs7 =
                   new DocumentFormat.OpenXml.Wordprocessing.FontSize();
                DocumentFormat.OpenXml.Wordprocessing.Color c7 =
                   new DocumentFormat.OpenXml.Wordprocessing.Color();
                DocumentFormat.OpenXml.Wordprocessing.Bold b7 =
                   new DocumentFormat.OpenXml.Wordprocessing.Bold();
                DocumentFormat.OpenXml.Wordprocessing.VerticalTextAlignment vta7 =
                   new DocumentFormat.OpenXml.Wordprocessing.VerticalTextAlignment();

                DocumentFormat.OpenXml.Wordprocessing.FontSize fs8 =
                   new DocumentFormat.OpenXml.Wordprocessing.FontSize();
                DocumentFormat.OpenXml.Wordprocessing.Color c8 =
                   new DocumentFormat.OpenXml.Wordprocessing.Color();
                DocumentFormat.OpenXml.Wordprocessing.Bold b8 =
                   new DocumentFormat.OpenXml.Wordprocessing.Bold();
                DocumentFormat.OpenXml.Wordprocessing.VerticalTextAlignment vta8 =
                   new DocumentFormat.OpenXml.Wordprocessing.VerticalTextAlignment();

                DocumentFormat.OpenXml.Wordprocessing.FontSize fs9 =
                   new DocumentFormat.OpenXml.Wordprocessing.FontSize();
                DocumentFormat.OpenXml.Wordprocessing.Color c9 =
                   new DocumentFormat.OpenXml.Wordprocessing.Color();
                DocumentFormat.OpenXml.Wordprocessing.Bold b9 =
                   new DocumentFormat.OpenXml.Wordprocessing.Bold();
                DocumentFormat.OpenXml.Wordprocessing.VerticalTextAlignment vta9 =
                   new DocumentFormat.OpenXml.Wordprocessing.VerticalTextAlignment();

                fs1.Val = "28";
                c1.Val = "black";
                b1.Val = true;
                vta1.Val = VerticalPositionValues.Baseline;
                runProperties1.Append(fs1);
                runProperties1.Append(c1);
                runProperties1.Append(b1);
                runProperties1.Append(vta1);
                run1.Append(runProperties1);
                run1.Append(new DocumentFormat.OpenXml.Wordprocessing.Text(name));

                fs2.Val = "28";
                c2.Val = "black";
                b2.Val = true;
                vta2.Val = VerticalPositionValues.Superscript;
                runProperties2.Append(fs2);
                runProperties2.Append(c2);
                runProperties2.Append(b2);
                runProperties2.Append(vta2);
                run1.Append(runProperties2);
                run1.Append(new DocumentFormat.OpenXml.Wordprocessing.Text(i.ToString()));

                fs3.Val = "28";
                c3.Val = "black";
                b3.Val = true;
                vta3.Val = VerticalPositionValues.Baseline;
                runProperties3.Append(fs3);
                runProperties3.Append(c3);
                runProperties3.Append(b3);
                runProperties3.Append(vta3);
                run1.Append(runProperties3);
                run1.Append(new DocumentFormat.OpenXml.Wordprocessing.Text()
                {
                    Text = autors.Count != i ? ", " : " ",
                    Space = SpaceProcessingModeValues.Preserve
                });

                fs4.Val = "28";
                c4.Val = "black";
                b4.Val = true;
                vta4.Val = VerticalPositionValues.Superscript;
                runProperties4.Append(fs4);
                runProperties4.Append(c4);
                runProperties4.Append(b4);
                runProperties4.Append(vta4);
                run2.Append(runProperties4);
                run2.Append(new DocumentFormat.OpenXml.Wordprocessing.Text(i.ToString()));

                fs5.Val = "28";
                c5.Val = "black";
                b5.Val = true;
                vta5.Val = VerticalPositionValues.Baseline;
                runProperties5.Append(fs5);
                runProperties5.Append(c5);
                runProperties5.Append(b5);
                runProperties5.Append(vta5);
                run2.Append(runProperties5);
                run2.Append(new DocumentFormat.OpenXml.Wordprocessing.Text(work));

                fs6.Val = "28";
                c6.Val = "black";
                b6.Val = true;
                vta6.Val = VerticalPositionValues.Baseline;
                runProperties6.Append(fs6);
                runProperties6.Append(c6);
                runProperties6.Append(b6);
                runProperties6.Append(vta6);
                run2.Append(runProperties6);
                run2.Append(new DocumentFormat.OpenXml.Wordprocessing.Text()
                {
                    Text = "; ",
                    Space = SpaceProcessingModeValues.Preserve
                });

                fs7.Val = "28";
                c7.Val = "black";
                b7.Val = false;
                vta7.Val = VerticalPositionValues.Superscript;
                runProperties7.Append(fs7);
                runProperties7.Append(c7);
                runProperties7.Append(b7);
                runProperties7.Append(vta7);
                run3.Append(runProperties7);
                run3.Append(new DocumentFormat.OpenXml.Wordprocessing.Text(i.ToString()));

                fs8.Val = "28";
                c8.Val = "black";
                b8.Val = false;
                vta8.Val = VerticalPositionValues.Baseline;
                runProperties8.Append(fs8);
                runProperties8.Append(c8);
                runProperties8.Append(b8);
                runProperties8.Append(vta8);
                run3.Append(runProperties8);
                run3.Append(new DocumentFormat.OpenXml.Wordprocessing.Text(value.Email));

                fs9.Val = "28";
                c9.Val = "black";
                b9.Val = false;
                vta9.Val = VerticalPositionValues.Baseline;
                runProperties9.Append(fs9);
                runProperties9.Append(c9);
                runProperties9.Append(b9);
                runProperties9.Append(vta9);
                run3.Append(runProperties9);
                run3.Append(new DocumentFormat.OpenXml.Wordprocessing.Text()
                {
                    Text = autors.Count != i ? ", " : " ",
                    Space = SpaceProcessingModeValues.Preserve
                });

                i++;
            });

            paragraph1 = new Paragraph(run1);
            body.AppendChild(paragraph1);
            paragraph2 = new Paragraph(run2);
            body.AppendChild(paragraph2);
            paragraph3 = new Paragraph(run3);
            body.AppendChild(paragraph3);
            body.Append(new W.Paragraph());
        }

        public void CreateNameDoc(string nameDoc)
        {
            CreateText("Calibri", "32", true, false, nameDoc.ToUpper(), 
                        false, 1, JustificationValues.Left);
            body.Append(new W.Paragraph());
        }

        /// <summary>
        /// Создает новый параграф и записывает туда текст.
        /// </summary>
        /// <param name="font">Название шрифта</param>
        /// <param name="fontSize">Размер шрифта</param>
        /// <param name="isBold">Жирный шрифт</param>
        /// <param name="isItalic">Курсив</param>
        /// <param name="targetText">Текст</param>
        /// <param name="isSpace">Табуляция</param>
        /// <param name="spaceMode"></param>
        /// <param name="justification">Выравнивание</param>
        /// <param name="otherFont">Доп шрифт</param>
        /// <param name="otherFontSize">Доп размер</param>
        /// <param name="otherIsBold">Доп жирность</param>
        /// <param name="otherIsItalic">Доп курсив</param>
        /// <param name="otherText">Доп текст</param>
        private void CreateText(string font, string fontSize, bool isBold, bool isItalic, string targetText, bool isSpace, 
            int spaceMode, JustificationValues justification, string otherFont = " ", string otherFontSize = " ", 
            bool otherIsBold = false, bool otherIsItalic = false, string otherText = " ")
        {
            W.ParagraphProperties pPr = new W.ParagraphProperties();
            W.Justification just = new W.Justification() { Val = justification };
            if (spaceMode == 0)
            {
                W.Indentation indentation = new W.Indentation() { FirstLine = "708"};
                pPr.Append(indentation);
            }
            
            pPr.Append(just);

            W.Paragraph para = body.AppendChild(new W.Paragraph(pPr));
            W.Run run = para.AppendChild(new W.Run());
            W.RunProperties runProperties = run.AppendChild(new W.RunProperties());

            W.Run run2 = para.AppendChild(new W.Run());
            W.RunProperties runProperties2 = run2.AppendChild(new W.RunProperties());

            W.RunFonts runFont = new W.RunFonts() { Ascii = font, HighAnsi = font };
            W.FontSize fonSize = new W.FontSize() { Val = fontSize };
            W.Bold bold = new W.Bold() { Val = OnOffValue.FromBoolean(isBold) };
            W.Italic italic = new W.Italic() { Val = OnOffValue.FromBoolean(isItalic) };
           

            W.Text text = new W.Text()
            {
                Text = targetText,
                Space = isSpace ? SpaceProcessingModeValues.Preserve : SpaceProcessingModeValues.Default
            };

            
            runProperties.AppendChild(runFont);
            runProperties.AppendChild(fonSize);
            runProperties.AppendChild(bold);
            runProperties.AppendChild(italic);

            if (!string.IsNullOrWhiteSpace(otherText))
            {
                W.RunFonts otherRunFont = new W.RunFonts() { Ascii = otherFont, HighAnsi = otherFont };
                W.FontSize otherFonSize = new W.FontSize() { Val = otherFontSize };
                W.Text otext = new W.Text(otherText);
                W.Bold obold = new W.Bold() { Val = OnOffValue.FromBoolean(otherIsBold) };
                W.Italic oitalic = new W.Italic() { Val = OnOffValue.FromBoolean(otherIsItalic) };

                runProperties2.AppendChild(otherRunFont);
                runProperties2.AppendChild(otherFonSize);
                runProperties2.AppendChild(obold);
                runProperties2.AppendChild(oitalic);

                run.Append(text);
                run2.Append(otext);
            }
            else
            {
                run.Append(text);
            }
        }

        public void save()
        {
            doc.Body = body;
            mainDoc.Document = doc;
            wDoc.Close();
        }
    }
}