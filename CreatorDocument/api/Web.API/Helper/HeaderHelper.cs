﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Drawing.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Vml.Spreadsheet;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Web.API.Helper
{
    public static class HeaderHelper
    {
        public static void ChangeHeader(String documentPath, int id)
        {
            // Replace header in target document with header of source document.
            using (WordprocessingDocument document = WordprocessingDocument.Open(documentPath, true))
            {
                // Get the main document part
                MainDocumentPart mainDocumentPart = document.MainDocumentPart;

                // Delete the existing header and footer parts
                mainDocumentPart.DeleteParts(mainDocumentPart.HeaderParts);
                mainDocumentPart.DeleteParts(mainDocumentPart.FooterParts);

                // Create a new header and footer part
                HeaderPart headerPart = mainDocumentPart.AddNewPart<HeaderPart>();
                FooterPart footerPart = mainDocumentPart.AddNewPart<FooterPart>();

                // Get Id of the headerPart and footer parts
                string headerPartId = mainDocumentPart.GetIdOfPart(headerPart);
                string footerPartId = mainDocumentPart.GetIdOfPart(footerPart);

                GenerateHeaderPartContent(headerPart, id);

                GenerateFooterPartContent(footerPart);

                // Get SectionProperties and Replace HeaderReference and FooterRefernce with new Id
                SectionProperties sectionProperties = new SectionProperties();
                sectionProperties.PrependChild<HeaderReference>(new HeaderReference() { Id = headerPartId });
                sectionProperties.PrependChild<FooterReference>(new FooterReference() { Id = footerPartId });

                mainDocumentPart.Document.Body.Append(sectionProperties);
            }
        }


        public static void GenerateHeaderPartContent(HeaderPart part, int id)
        {
            Header header1 = new Header() { MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "w14 wp14" } };
            header1.AddNamespaceDeclaration("wpc", "http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas");
            header1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            header1.AddNamespaceDeclaration("o", "urn:schemas-microsoft-com:office:office");
            header1.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
            header1.AddNamespaceDeclaration("m", "http://schemas.openxmlformats.org/officeDocument/2006/math");
            header1.AddNamespaceDeclaration("v", "urn:schemas-microsoft-com:vml");
            header1.AddNamespaceDeclaration("wp14", "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing");
            header1.AddNamespaceDeclaration("wp", "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing");
            header1.AddNamespaceDeclaration("w10", "urn:schemas-microsoft-com:office:word");
            header1.AddNamespaceDeclaration("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
            header1.AddNamespaceDeclaration("w14", "http://schemas.microsoft.com/office/word/2010/wordml");
            header1.AddNamespaceDeclaration("wpg", "http://schemas.microsoft.com/office/word/2010/wordprocessingGroup");
            header1.AddNamespaceDeclaration("wpi", "http://schemas.microsoft.com/office/word/2010/wordprocessingInk");
            header1.AddNamespaceDeclaration("wne", "http://schemas.microsoft.com/office/word/2006/wordml");
            header1.AddNamespaceDeclaration("wps", "http://schemas.microsoft.com/office/word/2010/wordprocessingShape");

            Paragraph paragraph1 = new Paragraph() { RsidParagraphAddition = "00164C17", RsidRunAdditionDefault = "00164C17" };
            Paragraph paragraph2 = new Paragraph() { RsidParagraphAddition = "00164C17", RsidRunAdditionDefault = "00164C17" };

            ParagraphProperties paragraphProperties2 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId2 = new ParagraphStyleId() { Val = "Footer" };
            Justification just2 = new Justification() { Val = JustificationValues.Center };
            paragraphProperties2.Append(paragraphStyleId2);
            paragraphProperties2.Append(just2);

            ParagraphProperties paragraphProperties1 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId1 = new ParagraphStyleId() { Val = "Header" };

            paragraphProperties1.Append(paragraphStyleId1);

            RunProperties rp = new RunProperties();
            Bold bold = new Bold() { Val = true };
            Italic italic = new Italic { Val = true };
            RunFonts runFont = new RunFonts() { Ascii = "Calibri", HighAnsi = "Calibri"};
            FontSize fonSize = new FontSize() { Val = "24" };

            RunProperties rp1 = new RunProperties();
            Bold bold2 = new Bold() { Val = true };
            RunFonts runFont2 = new RunFonts() { Ascii = "Calibri", HighAnsi = "Calibri" };
            FontSize fonSize2 = new FontSize() { Val = "28" };

            rp1.Append(bold2, runFont2, fonSize2);
            rp.Append(bold, italic, runFont, fonSize);

            Run run1 = new Run();
            Run run2 = new Run();
            Text text1 = new Text();
            text1.Text = $"Электронная библиотека КФУ. {DateTime.Now.Year}. T. {id}. № 1";
            Text text2 = new Text() { Text = "_____________________________________________________________________", Space = SpaceProcessingModeValues.Preserve };

            run2.Append(rp1);
            run2.Append(text2);

            run1.Append(rp);
            run1.Append(text1);

            paragraph2.Append(paragraphProperties2);
            paragraph2.Append(run2);
            paragraph1.Append(paragraphProperties1);
            paragraph1.Append(run1);

            header1.Append(paragraph1);
            header1.Append(paragraph2);

            part.Header = header1;
        }

        public static void GenerateFooterPartContent(FooterPart part)
        {
            Footer footer1 = new Footer() { MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "w14 wp14" } };
            footer1.AddNamespaceDeclaration("wpc", "http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas");
            footer1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            footer1.AddNamespaceDeclaration("o", "urn:schemas-microsoft-com:office:office");
            footer1.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
            footer1.AddNamespaceDeclaration("m", "http://schemas.openxmlformats.org/officeDocument/2006/math");
            footer1.AddNamespaceDeclaration("v", "urn:schemas-microsoft-com:vml");
            footer1.AddNamespaceDeclaration("wp14", "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing");
            footer1.AddNamespaceDeclaration("wp", "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing");
            footer1.AddNamespaceDeclaration("w10", "urn:schemas-microsoft-com:office:word");
            footer1.AddNamespaceDeclaration("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
            footer1.AddNamespaceDeclaration("w14", "http://schemas.microsoft.com/office/word/2010/wordml");
            footer1.AddNamespaceDeclaration("wpg", "http://schemas.microsoft.com/office/word/2010/wordprocessingGroup");
            footer1.AddNamespaceDeclaration("wpi", "http://schemas.microsoft.com/office/word/2010/wordprocessingInk");
            footer1.AddNamespaceDeclaration("wne", "http://schemas.microsoft.com/office/word/2006/wordml");
            footer1.AddNamespaceDeclaration("wps", "http://schemas.microsoft.com/office/word/2010/wordprocessingShape");

            Paragraph paragraph1 = new Paragraph() { RsidParagraphAddition = "00164C17", RsidRunAdditionDefault = "00164C17" };
            Paragraph paragraph2 = new Paragraph() { RsidParagraphAddition = "00164C17", RsidRunAdditionDefault = "00164C17" };

            ParagraphProperties paragraphProperties1 = new ParagraphProperties();
            ParagraphProperties paragraphProperties2 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId1 = new ParagraphStyleId() { Val = "Footer" };
            Justification just = new Justification() { Val = JustificationValues.Center };
            ParagraphStyleId paragraphStyleId2 = new ParagraphStyleId() { Val = "Footer" };
            Justification just2 = new Justification() { Val = JustificationValues.Center };
            paragraphProperties1.Append(paragraphStyleId1);
            paragraphProperties1.Append(just);
            paragraphProperties2.Append(paragraphStyleId2);
            paragraphProperties2.Append(just2);

            RunProperties rp = new RunProperties();
            RunProperties rp1 = new RunProperties();

            Bold bold = new Bold() { Val = true };
            RunFonts runFont = new RunFonts() { Ascii = "Calibri", HighAnsi = "Calibri" };
            FontSize fonSize = new FontSize() { Val = "28" };

            Bold bold2 = new Bold() { Val = true };
            RunFonts runFont2 = new RunFonts() { Ascii = "Calibri", HighAnsi = "Calibri" };
            FontSize fonSize2 = new FontSize() { Val = "28" };
            rp.Append(bold, runFont, fonSize);
            rp1.Append(bold2, runFont2, fonSize2);

            Run run1 = new Run();
            Run run2 = new Run();
            SimpleField simpleField = new SimpleField() { Instruction = @"PAGE   \* MERGEFORMAT" };
            Text text1 = new Text() { Text = "_____________________________________________________________________", Space = SpaceProcessingModeValues.Preserve };

            run1.Append(rp);
            run1.Append(simpleField);
            run2.Append(rp1);
            run2.Append(text1);

            paragraph2.Append(paragraphProperties2);
            paragraph2.Append(run2);
            paragraph1.Append(paragraphProperties1);
            paragraph1.Append(run1);

            footer1.Append(paragraph2);
            footer1.Append(paragraph1);

            part.Footer = footer1;
        }
    }
}
