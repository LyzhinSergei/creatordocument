﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;

namespace Web.API.Helper
{
    public class TranslateHelper
    {
        Dictionary<string, string> words = new Dictionary<string, string>();
        private static void init(TranslateHelper th)
        {
            th.words.Add("а", "a");
            th.words.Add("б", "b");
            th.words.Add("в", "v");
            th.words.Add("г", "g");
            th.words.Add("д", "d");
            th.words.Add("е", "e");
            th.words.Add("ё", "yo");
            th.words.Add("ж", "zh");
            th.words.Add("з", "z");
            th.words.Add("и", "i");
            th.words.Add("й", "j");
            th.words.Add("к", "k");
            th.words.Add("л", "l");
            th.words.Add("м", "m");
            th.words.Add("н", "n");
            th.words.Add("о", "o");
            th.words.Add("п", "p");
            th.words.Add("р", "r");
            th.words.Add("с", "s");
            th.words.Add("т", "t");
            th.words.Add("у", "u");
            th.words.Add("ф", "f");
            th.words.Add("х", "h");
            th.words.Add("ц", "c");
            th.words.Add("ч", "ch");
            th.words.Add("ш", "sh");
            th.words.Add("щ", "sch");
            th.words.Add("ъ", "j");
            th.words.Add("ы", "i");
            th.words.Add("ь", "j");
            th.words.Add("э", "e");
            th.words.Add("ю", "yu");
            th.words.Add("я", "ya");
            th.words.Add("А", "A");
            th.words.Add("Б", "B");
            th.words.Add("В", "V");
            th.words.Add("Г", "G");
            th.words.Add("Д", "D");
            th.words.Add("Е", "E");
            th.words.Add("Ё", "Yo");
            th.words.Add("Ж", "Zh");
            th.words.Add("З", "Z");
            th.words.Add("И", "I");
            th.words.Add("Й", "J");
            th.words.Add("К", "K");
            th.words.Add("Л", "L");
            th.words.Add("М", "M");
            th.words.Add("Н", "N");
            th.words.Add("О", "O");
            th.words.Add("П", "P");
            th.words.Add("Р", "R");
            th.words.Add("С", "S");
            th.words.Add("Т", "T");
            th.words.Add("У", "U");
            th.words.Add("Ф", "F");
            th.words.Add("Х", "H");
            th.words.Add("Ц", "C");
            th.words.Add("Ч", "Ch");
            th.words.Add("Ш", "Sh");
            th.words.Add("Щ", "Sch");
            th.words.Add("Ъ", "J");
            th.words.Add("Ы", "I");
            th.words.Add("Ь", "J");
            th.words.Add("Э", "E");
            th.words.Add("Ю", "Yu");
            th.words.Add("Я", "Ya");
            th.words.Add(" ", " ");
        }

        public static string Translite(string text)
        {
            TranslateHelper th = new TranslateHelper();
            init(th);
            string source = text;
            foreach (KeyValuePair<string, string> pair in th.words)
            {
                source = source.Replace(pair.Key, pair.Value);
            }
            return source;
        }

        public static string Translate(string word)
        {
            var toLanguage = "en";
            var fromLanguage = "ru";
            var url = $"https://translate.googleapis.com/translate_a/single?client=gtx&sl={fromLanguage}&tl={toLanguage}&dt=t&q={HttpUtility.UrlEncode(word)}";
            var webClient = new WebClient
            {
                Encoding = System.Text.Encoding.UTF8
            };
            var result = webClient.DownloadString(url);
            try
            {
                result = result.Substring(4, result.IndexOf("\"", 4, StringComparison.Ordinal) - 4);
                return result;
            }
            catch
            {
                return "Error";
            }
        }
    }
}
