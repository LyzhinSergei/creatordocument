﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Web.API.Common;

namespace InformationSearchBasics.Libs.TermFrequencyParamsCalculation.Commands
{
    public class TfCalculateCommand
    {
        private readonly string _docsFolderPath;

        public TfCalculateCommand(string docsFolderPath)
        {
            _docsFolderPath = docsFolderPath;
        }

        public IEnumerable<DocumentBasedFrequencyCalculationResult> Handle()
        {
            var result = new List<DocumentBasedFrequencyCalculationResult>();

            var fileText = File.ReadAllText(_docsFolderPath);

            var tfParamsList = fileText
                .Split(' ')
                .Where(w => !string.IsNullOrEmpty(w))
                .GroupBy(w => w)
                .Select(g => new DocumentBasedFrequencyCalculationResult(
                    g.Key,
                     Math.Round(g.Count() / (double)fileText.Length, 5),
                    "1doc"));

            result.AddRange(tfParamsList);
            

            return result.OrderByDescending(tfp => tfp.Value).Where(x => x.Term.Length > 4);
        }
    }
}
