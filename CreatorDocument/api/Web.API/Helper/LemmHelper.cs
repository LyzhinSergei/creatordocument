﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Web.API.Helper
{
    public class LemmHelper
    {
        public static void Lemm(string path)
        {
            var regex = new Regex(@"[\p{IsCyrillic}]+");
            var pathLem = path + "lem.txt";

            using (StreamReader sr = new StreamReader(path + "keywords.txt"))
            {
                var text = sr.ReadToEnd();

                var tokens = regex.Matches(text)
                    .Select(x => x.ToString())
                    .Where(x => !string.IsNullOrEmpty(x) && !string.IsNullOrWhiteSpace(x));

                var tokenized = string.Join(" ", tokens);

                var lemmas = new Lemmatizer.Lemmatizer(@"../api/Web.API/Helper/mystem/mystem.exe").LemmatizeText(tokenized)
                    .Trim()
                    .Replace("   ", " ");

                using (var stream = new FileStream(pathLem, FileMode.OpenOrCreate))
                {
                    var buffer = Encoding.UTF8.GetBytes(lemmas);
                    stream.Write(buffer, 0, buffer.Length);
                }
            }

        }
    }
}
