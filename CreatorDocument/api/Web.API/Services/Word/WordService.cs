﻿using DAL.Entities;
using Interfaces;
using System.Linq;
using DAL;
using System.IO;
using OMath;
using Web.API.Helper;
using InformationSearchBasics.Libs.TermFrequencyParamsCalculation.Commands;
using Web.API.Lemmatizer;

namespace Web.API.Services.Excel
{
    public class WordService : IWordSevice
    {
        private readonly EfContext _db;

        public WordService(EfContext db)
        {
            _db = db; 
        }

        public string ExportWord(Creator creator)
        {
            var pathDoc = Directory.GetCurrentDirectory() + "\\doc\\";
            var nameDoc = creator.Name;
            if(nameDoc.Length >= 50)
            {
                nameDoc = nameDoc.Substring(0, 49);
            }
            string pathDocument = pathDoc + nameDoc + ".docx";
            if (File.Exists(pathDocument))
            {
                pathDocument = pathDoc + nameDoc + "(1)"+ ".docx";
                if (File.Exists(pathDocument))
                {
                    File.Delete(pathDocument);
                }    
            }
            var id = creator.Id;
            var autors = creator.Autors;
            var annotation = creator.Annotation;
            var keys = creator.Keys;
            var introduction = creator.Introduction;
            var main = creator.Main;
            var conclusion = creator.Conclusion;
            var bibliography = creator.Bibliography;
            var nameDocEng = TranslateHelper.Translate(nameDoc);
            var annotationEng = creator.AnnotationEng;
            var keysEng = creator.KeysEng;
            var bibliographyEng = TranslateHelper.Translite(bibliography);

            if (string.IsNullOrEmpty(keys))
            {
                var pathKeyWords = pathDoc + "keywords.txt";
                using (var stream = new StreamWriter(pathKeyWords))
                {
                    stream.WriteLine(annotation);
                    stream.WriteLine(introduction);
                    foreach (var m in main)
                    {
                        stream.WriteLine(m.Name);
                        stream.WriteLine(m.Text);
                    }
                    stream.WriteLine(conclusion);
                };

                LemmHelper.Lemm(pathKeyWords);
                // Tf data prepared.
                var pathLem = pathDoc + "lem.txt";
                var tfCalcResult = new TfCalculate(pathLem).Handle().ToArray();
                var tfTableData = tfCalcResult
                    .Select(tfp => tfp.Term).Take(10);
                foreach (var word in tfTableData)
                {
                    keys += word + ", ";
                }
                keysEng = TranslateHelper.Translate(keys);
            }
            
            

            OpenWord ow = new OpenWord();
            ow.Init(pathDocument);
            ow.CreateNameDoc(nameDoc);
            ow.CreateNameAutors(autors,false);
            ow.CreateAnnotation(annotation, false);
            ow.CreateKeyWords(keys, false);
            ow.CreateIntroduction(introduction);
            ow.CreateMain(main);
            ow.CreateConclusion(conclusion);
            ow.CreateBibliography(bibliography, false);
            ow.CreateLine();
            ow.CreateNameDoc(nameDocEng);
            ow.CreateNameAutors(autors, true);
            ow.CreateAnnotation(annotationEng, true);
            ow.CreateKeyWords(keysEng, true);
            ow.CreateBibliography(bibliographyEng, true);
            ow.CreateAutorsDescription(autors);
            ow.save();
            HeaderHelper.ChangeHeader(pathDocument, id);

            return pathDocument;
        }
    }
}
