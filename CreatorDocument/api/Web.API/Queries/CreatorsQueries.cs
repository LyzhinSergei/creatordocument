﻿using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using DAL;
using DAL.Entities;
using Interfaces.Queries;
using Microsoft.EntityFrameworkCore;
using Web.API.Models;

namespace Web.API.Queries
{
    public class CreatorsQueries : ICreatorQueries
    {
        private readonly EfContext _db;

        public CreatorsQueries(EfContext db)
        {
            _db = db;
        }

        public async Task<CreatorModel> GetById(int id)
        {
            var res = await MapQuery(_db.Creator.Where(a => a.Id == id)).FirstOrDefaultAsync();
            return res;
        }

        public async Task<PageResultModel<CreatorModel>> GetList(CreatorFilter filter)
        {
            var query = _db.Creator.AsQueryable();

            if (filter.Status != null) 
                query = query.Where(x => x.Status == filter.Status);

            var list = await MapQuery(query).ToListAsync();

            return new PageResultModel<CreatorModel>
            {
                Size = filter.Size,
                Page = filter.Page,
                List = list
            };
        }

        private IQueryable<CreatorModel> MapQuery(IQueryable<Creator> query)
        {
            return query
                .Include(x => x.Autors)
                .Include(x => x.Main)
                .Select(x => new CreatorModel
                {
                    Id = x.Id,
                    Status = x.Status,
                    Link = x.Link,
                    Name = x.Name,
                    Bibliography = x.Bibliography,
                    CreatedDate = x.CreatedDate,
                    Annotation = x.Annotation,
                    AnnotationEng = x.AnnotationEng,
                    KeysEng = x.KeysEng,
                    Conclusion = x.Conclusion,
                    Introduction = x.Introduction,
                    Keys = x.Keys,
                    Autors = x.Autors.Select(y => new AutorsModel
                    {
                        Id = y.Id,
                        Email = y.Email,
                        Fullname = y.Name,
                        Work = y.Work,
                        Description = y.Description,
                        DescriptionEng = y.DescriptionEng,
                        PhotoUpload = _db.Uploads.Where(z => z.Id == y.PhotoId)
                                .Select(z => new PhotoUploadModel
                                {
                                    Id = z.Id,
                                    Extension = z.Extension,
                                    Height = z.Height,
                                    SizeInBytes = z.SizeInBytes,
                                    Width = z.Width
                                })
                                .FirstOrDefault(),
                    }).ToList(),
                    Main = x.Main.Select(m => new MainModel
                    {
                        Id = m.Id,
                        Name = m.Name,
                        Text = m.Text
                    }).ToList()

                });
        }
    }
}