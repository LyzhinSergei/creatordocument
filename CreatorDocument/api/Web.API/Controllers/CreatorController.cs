﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using FluentValidation;
using Interfaces;
using Interfaces.Contexts;
using Interfaces.Queries;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Web.API.Models;

namespace Web.API.Controllers
{
    [Route("api/creators")]
    [ApiController]
    public class CreatorController : ControllerBase
    {
        private readonly ICommandHandler _commandHandler;
        private readonly ICreatorQueries _creatorQueries;
        private readonly IValidatorFactory _validatorFactory;
        private readonly IWordSevice _wordSevice;

        public CreatorController(ICommandHandler commandHandler,
            IValidatorFactory validatorFactory,
            ICreatorQueries creatorQueries,
            IWordSevice wordSevice)
        {
            _commandHandler = commandHandler;
            _validatorFactory = validatorFactory;
            _creatorQueries = creatorQueries;
            _wordSevice = wordSevice;
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResultCode), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _commandHandler.Execute(new CreatorDeleteContext
            {
                Id = id
            });
            return result.IsSuccess ? (IActionResult) Ok() : BadRequest(new ApiResultCode(result));
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(CreatorModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResultCode), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Put(int id, [FromBody] CreatorModel model, int time_offset = 0)
        {
            var validationResult = _validatorFactory.GetValidator<CreatorModel>().Validate(model);
            if (!validationResult.IsValid) return BadRequest(new ApiResultCode(validationResult));

            var creator = GetCreator(model);
            creator.Link = _wordSevice.ExportWord(creator);

            var commandResult = await _commandHandler.Execute(new CreatorUpdateContext
            {
                Id = id,
                Creator = creator
            });
            if (!commandResult.IsSuccess) return BadRequest(new ApiResultCode(commandResult));
            var creatortModel = await _creatorQueries.GetById(id);
            return Ok(creatortModel);
        }

        [HttpPost("")]
        [ProducesResponseType(typeof(CreatorModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResultCode), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] CreatorModel model, int time_offset = 0)
        {
            var validationResult = _validatorFactory.GetValidator<CreatorModel>().Validate(model);
            if (!validationResult.IsValid) return BadRequest(new ApiResultCode(validationResult));

            var creator = GetCreator(model);
            creator.Link = _wordSevice.ExportWord(creator);
            creator.Status = Creator.Statuses.Active;
            var commandResult = await _commandHandler.Execute(new CreatorCreateContext
            {
                Creator = creator
            });

            if (!commandResult.IsSuccess) return BadRequest(new ApiResultCode(commandResult));

            var creatorModel = await _creatorQueries.GetById(model.Id);
            return Ok(creatorModel);
        }

        [HttpGet("GetList/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetLis(
            int page = 1,
            int size = 1000,
            int time_offset = 0,
            string tittle = null,
            int? id = null)
        {
            var result = await _creatorQueries.GetList(new CreatorFilter
            {
                Page = page,
                Size = size
            });

            return Ok(result);
        }

        [HttpGet("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetList(
            int page = 1,
            int size = 1000,
            string status = null,
            int time_offset = 0,
            string tittle = null)
        {
            var result = await _creatorQueries.GetList(new CreatorFilter
            {
                Status = status,
                Page = page,
                Size = size
            });

            return Ok(result);
        }


        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(int id, int time_offset = 0)
        {
            var creator = await _creatorQueries.GetById(id);
            return creator == null
                ? (IActionResult) BadRequest(new ApiResultCode("DOC_NOT_FOUND", "Документ не найден"))
                : Ok(creator);
        }


        private Creator GetCreator(CreatorModel model)
        {
            return new Creator
            {
                CreatedDate = DateTime.UtcNow,
                Id = model.Id,
                Status = model.Status,
                Name = model.Name,
                Bibliography = model.Bibliography,
                Annotation = model.Annotation,
                Conclusion = model.Conclusion,
                Introduction = model.Introduction,
                Keys = model.Keys,
                AnnotationEng = model.AnnotationEng,
                KeysEng = model.KeysEng,
                Autors = model.Autors.Select(y => new Autor
                {
                    Id = y.Id,
                    Email = y.Email,
                    Name = y.Fullname,
                    Work = y.Work,
                    Description = y.Description,
                    DescriptionEng = y.DescriptionEng,
                    PhotoId = y.PhotoUpload.Id,
                    Extension = y.PhotoUpload.Extension
                }).ToList(),
                Main = model.Main.Select(m => new Main
                {
                    Id = m.Id,
                    Name = m.Name,
                    Text = m.Text
                }).ToList()
            };
        }
    }
}