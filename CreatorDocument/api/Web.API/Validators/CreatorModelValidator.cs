﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.API.Models;

namespace Web.API.Validators
{
    public class CreatorModelValidator : AbstractValidator<CreatorModel>
    {
        public CreatorModelValidator()
        {
            RuleFor(x => x.Annotation)
                .MaximumLength(5000)
                .WithMessage("Описание до 5000 символов");
        }
    }
}