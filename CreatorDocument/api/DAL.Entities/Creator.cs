﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class Creator
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string Link { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Bibliography { get; set; }
        public string Conclusion { get; set; }
        public string Introduction { get; set; }
        public string Annotation { get; set; }
        public string AnnotationEng { get; set; }
        public string Keys { get; set; }
        public string KeysEng { get; set; }
        public List<Main> Main { get; set; } = new List<Main>();
        public List<Autor> Autors { get; set; } = new List<Autor>();


        public string Type { get; set; }

        public static class Statuses
        {
            public const string Active = "ACTIVE";
            public const string Deleted = "DELETED";
            public const string Blocked = "BLOCKED";

            public static string[] Available = new[]
            {
                Active,
                Deleted,
                Blocked
            };
        }
    }
}
