﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class CreatorMain
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public int CreatorId { get; set; }
    }
}
