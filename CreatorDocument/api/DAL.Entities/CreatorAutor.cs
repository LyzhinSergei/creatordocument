﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class CreatorAutor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Work { get; set; }
        public int CreatortId { get; set; }
    }
}
