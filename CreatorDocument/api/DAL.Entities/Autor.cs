﻿namespace DAL.Entities
{
    public class Autor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Work { get; set; }
        public string Description { get; set; }
        public string DescriptionEng { get; set; }
        public string PhotoId { get; set; }
        public string Extension { get; set; }
    }
}