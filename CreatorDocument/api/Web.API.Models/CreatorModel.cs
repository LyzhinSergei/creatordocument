﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;

namespace Web.API.Models
{
    public class CreatorModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Status { get; set; }
        public string Bibliography { get; set; }
        public string Conclusion { get; set; }
        public string Introduction { get; set; }
        public string Annotation { get; set; }
        public string AnnotationEng { get; set; }
        public string Keys { get; set; }
        public string KeysEng { get; set; }
        public List<MainModel> Main { get; set; }
        public List<AutorsModel> Autors { get; set; }
    }
}