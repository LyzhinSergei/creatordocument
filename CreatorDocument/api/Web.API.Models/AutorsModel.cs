﻿namespace Web.API.Models
{
    public class AutorsModel
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Work { get; set; }
        public string Description { get; set; }
        public string DescriptionEng { get; set; }
        public PhotoUploadModel PhotoUpload { get; set; }
    }
}