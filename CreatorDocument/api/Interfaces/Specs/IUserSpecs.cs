﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces.Specs
{
    public interface IUserSpecs
    {
        Task<SpecResult> IsUserEmailUnique(string email);

    }
}
