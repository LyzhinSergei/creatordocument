﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IWordSevice
    {
        string ExportWord(Creator creator);
    }
}
