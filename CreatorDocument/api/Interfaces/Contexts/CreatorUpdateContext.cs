﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Contexts
{
    public class CreatorUpdateContext: IContext
    {
        public int Id { get; set; }
        public Creator Creator { get; set; }
    }
}
