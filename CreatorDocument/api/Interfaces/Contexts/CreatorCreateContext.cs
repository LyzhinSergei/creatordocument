﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Entities;
using Web.API.Models;

namespace Interfaces.Contexts
{
    public class CreatorCreateContext :IContext
    {
        public Creator Creator { get; set; }
    }
}
